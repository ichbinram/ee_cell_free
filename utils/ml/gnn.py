import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.distributions import Uniform

class Gamma(nn.Module):
    """
    Gamma neural network module for processing the edge features.

    Parameters
    ----------
    params : dict
        A dictionary containing hyperparameters for the model.

    input_dim : int
        Input dimension for the input layer.

    output_dim : int
        Output dimension for the processed gamma.

    device : str, optional
        The device to run the model on (e.g., "cpu", "cuda"). 
        Default is "cpu".

    final_layer : bool, optional
        A boolean flag indicating if this is the final layer of the network. 
        Default is False.

    Attributes
    ----------
    conv1 : nn.Conv3d
        3D convolutional layer for the first part of the gamma processing.

    conv2 : nn.Conv3d
        3D convolutional layer for the second part of the gamma processing.

    ex : torch.Tensor
        Tensor representing the adjacency matrix (num_users x num_users).

    n_cat1 : int
        The number of categories for the first gamma processing.

    n_cat2 : int
        The number of categories for the second gamma processing.

    n_cat3 : int
        The number of categories for the third gamma processing.

    feature_dim : int
        The feature dimension used in the gamma processing.

    num_users : int
        The number of users in the network.

    final_layer : bool
        A boolean flag indicating if this is the final layer of the network.

    Methods
    -------
    forward(gamma_input)
        Forward pass of the gamma neural network module.

    postprocess_layer(conv_output)
        Helper function to postprocess the convolutional output.

    """
    def __init__(self, params, input_dim, output_dim, device="cpu", final_layer=False):
        super().__init__()
        self.conv1 = nn.Conv3d(input_dim, params["gamma_feature_dim"] * 4, (1, 1, 1)).to(device)
        self.conv2 = nn.Conv3d(params["gamma_feature_dim"] * 4, output_dim, (1, 1, 1)).to(device)
        self.ex = (torch.ones((params["num_users"], params["num_users"])) -
                   torch.eye(params["num_users"])).requires_grad_().to(device)
        self.n_cat1 = params["num_users"] - 1
        self.n_cat2 = params["num_users"] - 1
        self.n_cat3 = (params["num_users"] - 1) ** 2
        self.feature_dim = params["gamma_feature_dim"]
        self.num_users = params["num_users"]
        self.final_layer = final_layer

    def forward(self, gamma_input):
        def postprocess_layer(conv_output):
            features_c0 = conv_output[:, : self.feature_dim, :, :, :]
            features_c1 = self.ex @ conv_output[:, self.feature_dim: (self.feature_dim * 2), :, :, :] / self.n_cat1
            features_c2 = conv_output[:, (self.feature_dim * 2): (self.feature_dim * 3), :, :, :] @ self.ex / self.n_cat2
            features_c3 = (self.ex @ conv_output[:, (self.feature_dim * 3): (self.feature_dim * 4), :, :, :]
                           @ self.ex / self.n_cat3)
            return torch.cat((features_c0, features_c1, features_c2, features_c3), 1)

        r = F.relu(self.conv1(gamma_input))
        r = postprocess_layer(r)
        r = self.conv2(r)
        if not self.final_layer:
            r = postprocess_layer(F.relu(r))
        return r
    
class Phi(nn.Module):
    """
    Gamma neural network module for processing node features.

    Parameters
    ----------
    params : dict
        A dictionary containing hyperparameters for the model.

    input_dim : int
        Input dimension for the channel gains.

    output_dim : int
        Output dimension for the processed gamma.

    device : str, optional
        The device to run the model on (e.g., "cpu", "cuda"). Default is "cpu".

    final_layer : bool, optional
        A boolean flag indicating if this is the final layer of the network. 
        Default is False.

    Attributes
    ----------
    conv1 : nn.Conv3d
        3D convolutional layer for the first part of the gamma processing.

    conv2 : nn.Conv3d
        3D convolutional layer for the second part of the gamma processing.

    ex : torch.Tensor
        Tensor representing the adjecency matrix (num_users x num_users).

    n_cat1 : int
        The number of categories for the first gamma processing.

    n_cat2 : int
        The number of categories for the second gamma processing.

    n_cat3 : int
        The number of categories for the third gamma processing.

    feature_dim : int
        The feature dimension used in the gamma processing.

    num_users : int
        The number of users in the network.

    final_layer : bool
        A boolean flag indicating if this is the final layer of the network.

    Methods
    -------
    forward(gamma_input)
        Forward pass of the gamma neural network module.

    postprocess_layer(conv_output)
        Helper function to postprocess the convolutional output.

    """
    def __init__(self, params, input_dim, output_dim, device="cpu"):
        super().__init__()
        self.conv1 = nn.Conv3d(input_dim, params["phi_feature_dim"] * 4, (1, 1, 1)).to(device)
        self.conv2 = nn.Conv3d(params["phi_feature_dim"] * 4, output_dim, (1, 1, 1)).to(device)
        self.ex = (torch.ones((params["num_users"], params["num_users"])) -
                   torch.eye(params["num_users"])).requires_grad_().to(device)
        self.n_cat1 = params["num_users"] - 1
        self.n_cat2 = params["num_users"] - 1
        self.n_cat3 = (params["num_users"] - 1) ** 2
        self.feature_dim = params["phi_feature_dim"]
        self.num_users = params["num_users"]

    def forward(self, phi_input):
        def postprocess_layer(conv_output):
            features_c0 = conv_output[:, : self.feature_dim, :, :, :]
            features_c1 = self.ex @ conv_output[:, self.feature_dim: (self.feature_dim * 2), :, :, :] / self.n_cat1
            features_c2 = conv_output[:, (self.feature_dim * 2): (self.feature_dim * 3), :, :, :] @ self.ex / self.n_cat2
            features_c3 = (self.ex @ conv_output[:, (self.feature_dim * 3): (self.feature_dim * 4), :, :, :]
                           @ self.ex / self.n_cat3)
            return torch.cat((features_c0, features_c1, features_c2, features_c3), 1)

        r = F.relu(self.conv1(phi_input))
        r = postprocess_layer(r)
        r = self.conv2(r)
        r = postprocess_layer(r)
        return r
    
class MessagePassing(nn.Module):
    """
    Message passing layer for the Graph neural network (GNN).

    Parameters
    ----------
    params : dict
        A dictionary containing hyperparameters for the model.

    device : str, optional
        The device to run the model on (e.g., "cpu", "cuda"). Default is "cpu".

    Attributes
    ----------
    params : dict
        A dictionary containing hyperparameters for the model.

    device : str
        The device to run the model on (e.g., "cpu", "cuda").

    phi1 : Phi
        Phi neural network module for the first part of the Edge Processing.

    phi2 : Phi
        Phi neural network module for the second part of the Edge Processing.

    phi3 : Phi
        Phi neural network module for the third part of the Edge Processing.

    phi4 : Phi
        Phi neural network module for the fourth part of the Edge Processing.

    phi5 : Phi
        Phi neural network module for the fifth part of the Edge Processing.

    gamma1 : Gamma
        Gamma neural network module for the first part of the Node Processing.

    gamma2 : Gamma
        Gamma neural network module for the second part of the Node Processing.

    gamma3 : Gamma
        Gamma neural network module for the third part of the Node Processing.

    gamma4 : Gamma
        Gamma neural network module for the fourth part of the Node Processing.

    gamma5 : Gamma
        Gamma neural network module for the fifth part of the Node Processing.

    ex : torch.Tensor
        Tensor representing the adjecency matrix (num_aps x num_aps) with 1's on 
        off-diagonal elements and 0's on diagonal elements.

    Methods
    -------
    forward(channel_gains)
        Forward pass of the Message Passing Layer module.

    process_edge(edge_features, node_features, phi)
        Helper function to process the edges in the Message Passing Layer module.

    process_node(msgs, node_features, gamma)
        Helper function to process the nodes in the Message Passing Layer module.

    """
    def __init__(self, params, device="cpu"):
        super().__init__()
        self.params = params
        self.device = device
        phi_output_dim = params["phi_feature_dim"] * 4
        gamma_output_dim = params["gamma_feature_dim"] * 4
        if params["reduced_msg_input"]:
            phi_input_dim = gamma_output_dim * 1 + params["input_feature_dim"]
            phi_input_dim_init = params["input_feature_dim"] * 2
        else:
            phi_input_dim = gamma_output_dim * 2 + params["input_feature_dim"]
            phi_input_dim_init = 3 * params["input_feature_dim"]
        gamma_input_dim_init = params["input_feature_dim"] + phi_output_dim
        gamma_input_dim = gamma_output_dim + phi_output_dim

        self.phi1 = Phi(params, phi_input_dim_init, phi_output_dim, device)
        self.phi2 = Phi(params, phi_input_dim, phi_output_dim, device)
        self.phi3 = Phi(params, phi_input_dim, phi_output_dim, device)
        self.phi4 = Phi(params, phi_input_dim, phi_output_dim, device)
        self.phi5 = Phi(params, phi_input_dim, phi_output_dim, device)

        self.gamma1 = Gamma(params, gamma_input_dim_init, gamma_output_dim, device)
        self.gamma2 = Gamma(params, gamma_input_dim, gamma_output_dim, device)
        self.gamma3 = Gamma(params, gamma_input_dim, gamma_output_dim, device)
        self.gamma4 = Gamma(params, gamma_input_dim, gamma_output_dim, device)
        self.gamma5 = Gamma(params, gamma_input_dim, 1, device, final_layer=True)

        self.ex = (torch.ones((params["num_aps"], params["num_aps"])) -
                   torch.eye(params["num_aps"])).requires_grad_().to(device)

    def forward(self, channel_gains):
        def process_edge(edge_features, node_features, phi):
            phi_input = torch.cat([edge_features, node_features], dim=1)
            phi_output = phi(phi_input)
            return phi_output

        def process_node(msgs, node_features, gamma):
            agg_msgs = (self.ex @ msgs.transpose(dim0=2, dim1=3)).transpose(dim0=2, dim1=3) / (self.params["num_aps"] - 1)
            gamma_input = torch.cat([node_features, agg_msgs], dim=1)
            gamma_output = gamma(gamma_input)
            return gamma_output

        msgs = process_edge(channel_gains, channel_gains, self.phi1)
        r = process_node(msgs, channel_gains, self.gamma1)

        msgs = process_edge(channel_gains, r, self.phi2)
        r = process_node(msgs, r, self.gamma2)

        msgs = process_edge(channel_gains, r, self.phi3)
        r = process_node(msgs, r, self.gamma3)

        msgs = process_edge(channel_gains, r, self.phi4)
        r = process_node(msgs, r, self.gamma4)

        msgs = process_edge(channel_gains, r, self.phi5)
        r = process_node(msgs, r, self.gamma5)

        r = torch.diagonal(r, 0, dim1=3, dim2=4)
        return r
    
class GNNGlobal(nn.Module):
    """
    Graph Neural Network (GNN) Global module for sum rate/ee maximization.

    Parameters
    ----------
    params : dict
        A dictionary containing hyperparameters for the model.

    device : str
        The device to run the model on (e.g., "cpu", "cuda").

    Attributes
    ----------
    params : dict
        A dictionary containing hyperparameters for the model.

    device : str
        The device to run the model on (e.g., "cpu", "cuda").

    gnn_a : MessagePassing
        Message passing module for processing channel gains 
        (for lower bound).

    gnn_l : MessagePassing
        Message Passing module for processing channel gains 
        (for upper bound).

    softplus : nn.Softplus
        Softplus activation function.

    Methods
    -------
    forward(gnn_input)
        Forward pass of the GNNGlobal module.

    """
    def __init__(self, params, device):
        super().__init__()
        self.params = params
        self.gnn_a = MessagePassing(params, device)
        self.gnn_l = MessagePassing(params, device)
        self.softplus = nn.Softplus()

    def forward(self, gnn_input):
        a = self.softplus(self.gnn_a(gnn_input) - 2)
        if self.params["objective"] in ('sr', 'pr'):
            l = self.softplus(self.gnn_l(gnn_input) + 4) * self.gnn_a.params["pmax"] + 1e-4
        elif self.params["objective"]=='ee':
            l = self.softplus(self.gnn_l(gnn_input)+4) + 1e-4
        b = a + l

        dis = Uniform(a, b)
        p = dis.rsample()
        sum_power = torch.maximum(p.sum(dim=3, keepdims=True), 
                                  torch.ones(1).to(self.gnn_l.device))
        if self.params["objective"] in ('sr','pr'):
            p = p / sum_power * self.gnn_a.params["pmax"]

        support = l.sum(dim=[1, 2, 3])
        return p, support