import numpy as np
from params import params

# global variables
noise = params["noise_power"]

# helper functions

def sinr_user( K, channel, power, noise):
    """Computes the SINR of a given configuration of channels, power and 
        noise with L APs and K UEs

    Parameters
    ----------
    channel : np.ndarray
        Effective channel matrix after precoding/beamforming. Shape: (L,n,K,K).
    power : np.ndarray
        power matrix containing power allocation per AP per UE. Shape:(L,n,K)
    K : int
        number of User equipments
    noise : float64
        noise power of the channel.

    Returns
    -------
    sinr : np.ndarray   
        SINR per user equipment. Shape:(K,1)
    """    
    #------------- received power per AP ------------------#
    rss = [c * p[:,np.newaxis,:] for c, p in zip(channel, power)]

    #------------ received power per UE ------------------#    
    rss = np.sum(np.stack(rss, axis=0), axis=0)

    #-------------- SINR per UE --------------------------#
    sinr = [rss[:, user_idx, user_idx] / (np.sum(rss[:, user_idx, :],axis=1) - 
                                        rss[:, user_idx, user_idx] + noise) 
                                        for user_idx in range(K)]

    return np.stack(sinr, axis=-1)

def sumrate(K, channel, power, noise):
    sinr = sinr_user(K, channel, power, noise)
    sum_rate = np.sum(np.log2(1+sinr), axis=-1)
    return sum_rate

def prod_rate(K, channel, power, noise):
    sinr = sinr_user(K, channel, power, noise)
    productrate = np.prod(np.log2(1+sinr), axis=-1, dtype=float)
    return productrate

def sum_ee(K, channel, power, const_power, noise):
    sinr = sinr_user(K, channel, power, noise)
    rate = np.log2(1+sinr)
    sum_power = np.sum(power, axis=0)
    return np.sum(rate/(const_power+sum_power), axis=-1)
