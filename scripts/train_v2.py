import os, argparse, datetime
from collections import deque
from pathlib import Path

import torch
import torch.optim as optim
from torch.utils.data import DataLoader
from torch.optim.lr_scheduler import ReduceLROnPlateau
import numpy as np

from utils.DataGeneration.CFData import CFData
from utils.ml.core import sumrate, prodrate, sum_ee, determine_kappa
from utils.ml.core import CFData as CFDataML
from utils.ml.gnn import GNNGlobal
from utils.Optimization.DataLoader import NpzDataLoader
from params import params

torch.set_default_dtype(torch.float32)
tb = True
try:
    from tensorboardX import SummaryWriter
except:
    tb = False
record = False and tb

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--device")
    parser.add_argument("--record")
    parser.add_argument("--num_data_samples", type=int)
    # if data is present then compute the number of chunks based on the available data else use the number of chunks provided
    group = parser.add_mutually_exclusive_group()
    group.add_argument("--num_chunks", type=int)
    group.add_argument("--dataset_path")
    args = parser.parse_args()

    path = f'data/chunks_{params["num_users"]}ue_{params["num_aps"]}ap_{params["num_antennas"]}ant' # initialize the path variable to a default path

    # in order to record the training behaviour with tensorboard
    if args.record is not None:
        record = args.record == "True"
    tb = tb and record

    torch.set_default_dtype(torch.float32)

    # selects the device to run the machine learning algorithm from
    if args.device is not None:
        device = args.device
    else:
        device = 'cuda' if torch.cuda.is_available() else 'cpu'

    # record training data 
    if record:
        now = datetime.datetime.now()
        dt_string = now.strftime("%d-%m-%Y_%H-%M-%S")
        Path(params["results_path"] + dt_string).mkdir(parents=True, exist_ok=True)
        params["results_path"] = params["results_path"] + dt_string + "/"

    # if number of samples is passed as an argument, use that data instead of the default one present in the params file
    if args.num_data_samples is not None:
        params["num_data_samples"] = int(args.num_data_samples)

    # if the dataset path is passed as an argument, load data from the specified path
    if args.dataset_path is not None:
        path = args.dataset_path
        params["data_available"] = True
        chunks = len(os.listdir(path))
        if chunks == 0:
            print(f'data not available at the specified directory {path}')
        else:
            params["num_samples_chunks"] = np.ceil(params["num_data_samples"]/chunks)
            mean_data = (NpzDataLoader(args.dataset_path+f'/chunk_{i}.npz') for i in range(chunks))
            all_chan = np.stack([data.effective_channels for data in mean_data])
            params["mean_channel"] = all_chan.mean()
            params["std_channel"] = all_chan.std()
            del all_chan
        
    # if number of chunks is passed as an argument, then compute the number of samples per chunk and generate data in chunks
    if args.num_chunks is not None:
        params["num_samples_chunks"] = int(np.ceil(params["num_data_samples"]/
                                                   int(args.num_chunks)))
        params["data_available"] = False
        data_gen = CFData(params)
        params["mean_rate_requirements"] = data_gen.rate_requirements.mean()
        if data_gen.rate_requirements.std() != 0:
            params["std_rate_requirements"] = data_gen.rate_requirements.std()  
        else: params["std_rate_requirements"] = 1
        data_gen.save_as_chunks(params["num_samples_chunks"])
        del data_gen
    params["input_feature_dim"] = 1
    model = GNNGlobal(params, device)

    # Debug
    # model.load_state_dict(torch.load("results/sumlograte/model_54000"))

    dataset = CFDataML(params, path=path, device=device)
    dataset_testing = CFDataML(params, path=path + "_testing", device=device, 
                             test=True)
    
    train_loader = DataLoader(dataset=dataset, batch_size=params['batch_size'], 
                              shuffle=True)
    optimizer = optim.SGD(model.parameters(), params["lr"])
    scheduler = ReduceLROnPlateau(optimizer=optimizer,
                                  mode="min",
                                  factor=0.9,
                                  patience=2000,
                                  cooldown=4000,
                                  min_lr=params["min_lr"])
    model.train()
    counter = 1
    kappa = torch.zeros(len(dataset)).to(device)
    previous_kappa = torch.zeros(len(dataset)).to(device)
    support_history = [deque() for _ in range(len(dataset))]
    print('training started..')
    if tb:
        writer = SummaryWriter(logdir=params["results_path"])

    for _iter in range(params["epoch"]):
        for indices, channels, required_rates, selection in train_loader:
            optimizer.zero_grad()
            power, support = model((channels - params["mean_channel"]) / 
                                         params["std_channel"])

            for i,channel_idx in enumerate(indices):
                while len(support_history[channel_idx]) > 30:
                    support_history[channel_idx].popleft()

                kappa[channel_idx] = determine_kappa(support_history[channel_idx],
                                                      previous_kappa[channel_idx])
                support_history[channel_idx].append(support[i].detach().cpu().numpy())
            previous_kappa = kappa

            if params["objective"] == 'sr':
                tb_obj = 'sr'
                objective = sumrate(channels, power, params)
            elif params["objective"] == 'pr':
                tb_obj = 'pr'
                objective = torch.log(prodrate(channels, power, params))
            elif params["objective"] == 'ee':
                tb_obj = 'ee'
                objective = sum_ee(channels, power, params)
            else:
                print('unknown objective')
                break

            loss = (-objective + kappa[indices] * support).mean()
            loss.backward()
            optimizer.step()

            if counter > 5e4:
                scheduler.step(loss)

            with torch.no_grad():
                channels = dataset_testing.channels[:, :, :, :, :]
                sparse_channels = dataset_testing.sparse_channels[:, :, :, :, :]
                power, support = model((sparse_channels - 
                                              params["mean_channel"]) / 
                                              params["std_channel"])
            obj_test = 0
            if params["objective"] == 'sr':
                obj_test = sumrate(channels, power, params)
            elif params["objective"] == 'pr':
                obj_test = torch.log(prodrate(channels, power, params))
            elif params["objective"] == 'ee':
                obj_test = sum_ee(channels, power, params) 

            if tb:
                writer.add_scalar("Training/"+tb_obj, 
                                  objective.mean().item(), counter)
                writer.add_scalar("Training/support", 
                                  support.mean().item(), counter)
                writer.add_scalar("Training/kappa", kappa.mean().item(), 
                                  counter)
                writer.add_scalar("Training/lr", 
                                  optimizer.param_groups[0]["lr"], counter)
                writer.add_scalar("Testing/"+tb_obj, 
                                  obj_test.mean().item(), counter)
            counter += 1

            if record and counter % 1000 == 0:
                torch.save(model.state_dict(), params["results_path"] +\
                            "model_{iter}".format(iter=counter))
                if record:
                    writer.flush()

        print(f'Iteration:{_iter}, loss:{loss.item()}, support:{support.mean().item()}')

            