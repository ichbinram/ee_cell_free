from abc import ABC, abstractmethod
from dataclasses import dataclass

import numpy as np
import cvxpy as cp
from cvxpy import expressions

from utils.metrics import sinr_user
from params import params


# global variables
noise = params["noise_power"]

@dataclass
class SurrogateFunction(ABC):
    """An abstract class to represent a surrogate function for different metrics
    to be used in SCA framework of Optimization. 
    """    
    channels: np.ndarray=None
    initial_power: np.ndarray=None
    opt_power: expressions.variable.Variable = None
    noise:float = noise
    

    def __post_init__(self):
        self.approximation = None

    @abstractmethod
    def set_approximation(self):
        """ Compute and return the approximation function.
        """        
        pass
    
    def get_approximation(self):
        """returns the approximation. It could be overridden if there is some
        post processing that is need to be done.
        """        
        return self.approximation
    
    @abstractmethod
    def update_approximation(self, **kwargs):
        """Update the approximation parameters and compute the new approximation.
        """        
        pass

class FirstOrderRateApprox(SurrogateFunction):
    """Inherits from the Abstract Surrogate function class. Approximation of 
    rate per user using the first order Taylor series.
    """    
    def set_approximation(self):
        self.L, self.K, _ = self.channels.shape
        self.log_2 = np.log(2)
        gamma_0 = sinr_user(self.K, self.channels[:,None,:,:], 
                            self.initial_power, self.noise)
        gamma_0 = np.squeeze(gamma_0, axis=0)
        a = gamma_0/(1+gamma_0)
        b = np.log2(1+gamma_0) - a*np.log2(gamma_0)

        self.norm_channels = self.channels/self.noise
        gain_approx = self.compute_gain()
        interference_approx = self.compute_interference()
        approx = cp.multiply(a,(gain_approx-interference_approx)) + b

        return approx
    
    def update_approximation(self, channels= None, x_0=None, opt=None):
        self.initial_power = x_0
        self.channels = channels
        self.opt_power = opt
        self.approximation = self.set_approximation()
    
    def compute_gain(self):
        """Compute the gain approximation.

        Returns
        -------
        cp.array
            An array containing gain approximations.
        """        
        self.alpha = np.diagonal(self.norm_channels, axis1=-2, axis2=-1)
        _pow = cp.reshape(self.opt_power,(self.L, self.K), order='C')
        gain = cp.multiply(self.alpha, _pow)
        gain_approx = cp.log(cp.sum(gain, axis=0))/self.log_2
        return gain_approx

    def compute_interference(self):
        """Compute the interference approximation.

        Returns
        -------
        cp.array
            An array containing interference approximation.
        """        
        index = np.arange(self.K)
        alpha3D = np.zeros_like(self.norm_channels)
        alpha3D[:,index,index] = self.alpha
        beta = self.norm_channels - alpha3D
        ifn = np.sum(np.sum(beta*self.initial_power, axis=-1), axis=0)
        const = np.log2(1+ifn)

        lin_term = cp.vstack([cp.multiply(beta[l],
                                          self.opt_power[None,l*self.K:(l+1)*self.K])
                                          for l in range(self.L)])
        lin_term = cp.sum(lin_term, axis=1)
        lin_term = cp.reshape(lin_term, (self.L, self.K), order='C')
        lin_term = cp.sum(lin_term, axis=0)

        inter_approx = const + (lin_term - ifn)/ (self.log_2*(1+ifn))
        return inter_approx
    
class SumRateApprox(FirstOrderRateApprox):
    """Approximation of sum rate function based on the first order rate 
    approximation.
    """    
    def get_approximation(self):
        self.approximation = cp.sum(self.approximation)
        return self.approximation
    
class ProductRateApprox(FirstOrderRateApprox):
    """Approximation of Log product rate function based on the first order rate
    approximation.
    """    
    def get_approximation(self):
        self.approximation = cp.sum(cp.log(self.approximation))
        return self.approximation
    
@dataclass
class SumEEApprox(FirstOrderRateApprox):
    """Approximation of sum energy efficiency of the users based on the first 
    order rate approximation. Uses Dinkelbach transform to convert the fraction
    to a non fractional program.
    """    
    pc: float = 1
    def update_approximation(self, channels=None, x_0=None, opt=None, t=1e-3):
        self.initial_power = x_0
        self.channels = channels
        self.opt_power = opt
        self.t = t
        self.rate = self.set_approximation()
        # self.sumrate = cp.sum(self.rate)
        power = cp.reshape(self.opt_power, (self.L, self.K), order='C')
        self.power_consumption = self.pc + cp.sum(power, axis=0)

    def get_approximation(self):
        self.approximation = self.rate\
              - cp.multiply((self.power_consumption), self.t)
        return cp.sum(self.approximation)