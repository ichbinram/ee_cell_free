from utils.DataGeneration.CFData import CFData
from params import params

# Configuration
params["num_aps"] = 5
params["num_users"] = 10
params["num_antennas"] = 5
params["square_length"] = 100

# Sample & chunk sizes
params["num_data_samples"] = 1024
params["num_samples_chunks"] = 1024

if __name__ == '__main__':
        dataset = CFData(params)
        dataset.save_as_chunks(params["num_samples_chunks"], path='data/chunks_10ue_5ap_5ant_testing')
        