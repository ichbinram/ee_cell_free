# External libraries
import numpy as np
import scipy as sp


#####################################################
##-------------------- Globals --------------------##
#####################################################
alpha = 3.67            # Pathloss exponent
sigma_sf = 4           # Standard deviation of Shadow fading
constant_term = -30.5   # Average channel gain (in dB) at reference distance of 1m [->corresponds to -148.1 dB at 1km, with alpha=3.76]
frequency = 13          # Operating frequency in GHz

#####################################################
##-------------------- Helpers --------------------##
#####################################################
def gains(dist) -> np.ndarray:
    """ Compute the large scale fading based on distances between UE and AP.

    Parameters
    ----------
    dist : np.ndarray
        Matrix containing distance between UE and AP.

    Returns
    -------
    np.ndarray
        A matrix containing the large scale fading between each UE and AP.
    """    
    # return constant_term - alpha*10*np.log10(dist) + sigma_sf * np.random.randn(*dist.shape)
    gains = np.minimum(-80, constant_term - 10 * alpha * np.log10(dist) 
                            - 20*np.log10(frequency/2)
                            + sigma_sf * np.random.randn(*dist.shape))
    return gains

def gen_ue_locations(num_ues: int, square_length: int) -> np.ndarray:
    """ Generate number of random points inside a square of given
        length [in m] on a cartesian plane.

    Parameters
    ----------
    num : int
        number of points to be generated.
    square_length : int
        Length of the square [in m].

    Returns
    -------
    np.ndarray
        A matrix of dimension (num,2), where each row represents a point and column 0 represents the x coordinate and column 1 represents y coordinate.
    """    
    loc = np.random.uniform(0, 1, (num_ues, 2)) * square_length
    return loc

def gen_ap_locations(num_aps: int, square_length: int) -> np.ndarray:
    """ Generate a number of points inside a square of given
        length [in m] on a cartesian plane. The points located
        on a square mesh, fitting inside the square.
    

    Parameters
    ----------
    num_aps : int
        number of points to be generated.
    square_length : int
        length of the square [in m].

    Returns
    -------
    np.ndarray
        A matrix of dimension (num,2), where each row represents a point
        and column 0 represents the x coordinate and column 1 represents
        y coordinate.
    """    
    num_x = int(np.ceil(np.sqrt(num_aps)))
    num_y = int(np.ceil(num_aps / num_x))
    
    dist_x = square_length / (num_x+ 1)
    dist_y = square_length / (num_y + 1)
    
    x = np.linspace((dist_x / 2) / square_length, 1 - ((dist_x / 2) / square_length), num_x) * square_length
    y = np.linspace((dist_y / 2) / square_length, 1 - ((dist_y / 2) / square_length), num_y) * square_length
    x, y = np.meshgrid(x, y)
    x = np.reshape(x, (-1, 1))[:num_aps, :]
    y = np.reshape(y, (-1, 1))[:num_aps, :]
    return np.concatenate((x, y), axis=1)

def compute_dist(pos1: np.ndarray, pos2: np.ndarray) -> np.ndarray:
    """ A function to compute the distance between points on a cartesian plane.

    Parameters
    ----------
    pos1 : np.ndarray
        Coordinates of point 1 (x,y)
    pos2 : np.ndarray
        Coordinates of point 2 (x,y)

    Returns
    -------
    np.ndarray
        Array containing distances between points.
    """    
    dist = sp.spatial.distance.cdist(pos1, pos2)
    dist = np.sqrt(100 + dist ** 2)
    return dist