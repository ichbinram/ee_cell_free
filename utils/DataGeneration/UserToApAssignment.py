# Standard libraries
from abc import ABC, abstractmethod
import sys

# External libraries
import numpy as np

# Custom libraries
from utils.helpers import create_bit_sequences, reshape_bit_sequences

class AssignmentProblemSolutionStrategy(ABC):
    """ Class, which offers multiple solution approaches to a given assignment problem from Communication System Design.
        The assignment problem can also be called a Many-to-Many Matching Problem.

        It provides some solution procedures for stable assignments of UEs to APs (e.g. there is the 'Domination' algorithm and
        the Generalized Serial Dictatorship (GSD) algorithm).
        It requires a complete matrix of channel gains between the UEs as rows and the APs as columns.
    """    
    @abstractmethod
    def __init__(self, data: tuple[np.ndarray, int, int]):
        """ Instance initialization.

        Parameters
        ----------
        channel_matrix : np.ndarray
            The given channel gains as matrix. UEs as rows & APs as columns
        p : int/np.ndarray of ints
            Quota of the UEs (Minimum number of assignments)
        q : int/np.ndarray of ints
            Quota of the APs (Maximum number of assignments)

        Raises
        ------
        ValueError
            If dimensions do not match
        """        
        # Initialize members
        self.channels = data[0]
        self.dims = data[0].shape
        self.min_columnplayers_per_row = data[1]
        self.max_rowplayers_per_column = data[2]
        self.max_columnplayers_per_row = [2] * self.dims[0]

        # assert the dimension
        assert (len(self.min_columnplayers_per_row) == self.dims[0]) and (len(self.max_rowplayers_per_column) == self.dims[1]), "Different dimensions of quotas and channel-matrix!!"

    @abstractmethod
    def solve(self) -> np.ndarray:
        """ Abstract method name to solve the underlying Many-to-Many matching problem.

        Returns
        -------
        np.ndarray
            The selection (adjacency) matrix (UEs as rows, APs as columns). Same shape as the channel_gain input matrix
        """        
        
    def _gsd_algorithm(self, channels, user_order) -> np.ndarray:
        """ Implementation of the Generalized Serial Dictatorship (GSD) algorithm, which returns ONLY ONE (arg: user_order)
            Pareto-Optimal-Matching (POM) from a Many-to-Many Matching problem.

        Parameters
        ----------
        channels: np.ndarray
            The (channel-)matrix to perform the algorithm on.
        user_order : np.ndarray
            A random permutation.

        Returns
        -------
        np.ndarray
            The selection matrix (UEs as rows, APs as columns). Not used channels are set to 0.
            Same shape as the channel_gain input matrix.
        """        
        assignable_channels = channels.copy()
        assigned_channels = np.zeros(self.dims)

        # 2) In each round:
        #   2.a) Have a chosen UE u
        #   2.b) u chooses himself it's most preferred AP, which is not full
        #   2.c) If there was no such assignment possible, remove u from further rounds
        while len(user_order) != 0:     # as long as we have UEs, which has open slots..
            for user in user_order:                                                         # 2.a)
                if np.max(assignable_channels[user]) == -np.inf:                            # 2.c) if there are no available APs
                    np.delete(user_order, user)                                             # remove this user from further consideration
                    continue                                                                # and continue with the next one
                
                preferred_columnplayer = np.argmax(assignable_channels[user])               # 2.b) choose a preferred AP and select its channel
                assigned_channels[user, preferred_columnplayer] = 1
                assignable_channels[user, preferred_columnplayer] = -np.inf                 # Remove this channel, so it can not be chosen again by the user
                
                if (np.sum(assigned_channels, axis=0)[preferred_columnplayer] ==            # If the chosen AP has reached its max. UEs:
                        self.max_rowplayers_per_column[preferred_columnplayer]): 
                    assignable_channels.T[preferred_columnplayer] = -np.inf                 # remove it from further consideration
                if np.all(np.isneginf(assignable_channels)):                                # Termination criterion: 
                    user_order = []                                                         # remove all channels
                    break                                                                   # and terminate

        return assigned_channels

    
    ############################################################################
    ##-------------------- Class representation functions --------------------##
    ############################################################################
    def __repr__(self):
        return str(self.channels)

    def __str__(self):
        return str(self.channels)

class ValidMatchings(AssignmentProblemSolutionStrategy):
    
    def __init__(self, data: tuple[np.ndarray, int, int]):
        super().__init__(data)
    
    def solve(self) -> np.ndarray:
        valid_matchings = []
        
        all_bit_sequences = create_bit_sequences(self.dims[0]*self.dims[1])
        all_bit_arrays = reshape_bit_sequences(all_bit_sequences, self.dims[0], self.dims[1])
        
        for matching in all_bit_arrays:
            if self.is_valid_matching(matching):
                valid_matchings.append(matching)
                
        return valid_matchings
                
    def is_valid_matching(self, matching: np.ndarray):
        if (np.sum(matching, axis=1) > self.max_rowplayers_per_column).any():
            return False
        if (np.sum(matching, axis=0) < self.min_columnplayers_per_row).any():
            return False
        return True

class ALL(AssignmentProblemSolutionStrategy):
    """ Placeholder class; does not implement new behavior, it just returns a full matching."""
    
    def __init__(self, data: tuple[np.ndarray, int, int]):
        super().__init__(data)
        
    def solve(self) -> np.ndarray:
        return np.ones(self.dims)
    
class GSDR(AssignmentProblemSolutionStrategy):
    """ 'Generalized Serial Dictatorship'-Strategy with Random user-order."""
    
    def __init__(self, data: tuple[np.ndarray, int, int]):
        super().__init__(data)
        
    def solve(self):
        perm = np.random.permutation(np.arange(self.dims[0]))
        return self._gsd_algorithm(self.channels, perm)
    
    
class DOM(AssignmentProblemSolutionStrategy):
    """ Concrete class containing the 'Mu-row-optimal'-Strategy. """
    
    def __init__(self, data: tuple[np.ndarray, int, int]):
        super().__init__(data)
        
    def solve(self):
        sel_gains = self._row_optimal_domination_algorithm()
        adj_mat = np.not_equal(sel_gains, 0).astype(int)
        return adj_mat
    
    def _row_optimal_domination_algorithm(self):
        """ Implementation of the Mu-row-optimal algorithm for calculating a stable assignment for a Many-to-Many
            assignment problem.

        Returns
        -------
        np.ndarray
            The selection matrix (UEs as rows, APs as columns). Not used channels are set to 0.
            Same shape as the channel_gain input matrix.
        """        
        # get rankings of channels
        row_ranking = np.zeros(self.dims)
        col_ordering = np.zeros(self.dims)
        row_ranking, _, _, col_ordering = self._get_ranks()

        # find a (full-dimensional) matrix of the row-best values (non-row-best nodes are set to 0)
        row_best = self._get_row_opt(row_ranking)
        # remove column-dominated nodes
        coldom_removed = self._remove_column_dominated(row_best, col_ordering)
        
        # Now we need to fulfil maximum quotas!! Choose only a subset of APs for each UE.
        # Therefore, just use the GSD-algorithm!!
        order = np.arange(self.dims[0])
        assigned_channels = self._gsd_algorithm(coldom_removed, order)
            
        return assigned_channels
    
    def _column_optimal_domination_algorithm(self):
        """ Implementation of the Mu-column-optimal algorithm for calculating a stable assignment for a Many-to-Many
            assignment problem.

        Returns
        -------
        np.ndarray
            The selection matrix (UEs as rows, APs as columns). Not used channels are set to 0.
            Same shape as the channel_gain input matrix.
        """        
        # get rankings of channels
        row_ordering = np.zeros(self.dims)
        col_ranking = np.zeros(self.dims)
        _, col_ranking, row_ordering, _ = self._get_ranks()

        # find a (full-dimensional) matrix of the column-best values (non-row-best nodes are set to 0)
        col_best = self._get_col_opt(col_ranking)
        # remove row-dominated nodes
        rowdom_removed = self._remove_row_dominated(col_best, row_ordering)
        
        # Now we need to fulfil maximum quotas!! Choose only a subset of APs for each UE.
        # Therefore, just use the GSD-algorithm!!
        order = np.arange(self.dims[0])
        assigned_channels = self._gsd_algorithm(rowdom_removed, order)

        return assigned_channels
    
    def _remove_column_dominated(self, row_best, col_ordering):
        """ Removes column-dominated nodes from a channels-matrix-copy.

        Parameters
        ----------
        row_best : list of lists
            The row-optimal nodes as in a list for each row.
        col_ordering : np.ndarray
            The column-ordering.
        """
        removed = self.channels.copy()    # Create a return matrix
        
        for col in range(self.dims[1]):                     # in each column c: O(|C|)
            for row in reversed(col_ordering.T[col]):           # get the next-worst channel in this column
                if (self.channels[row, col] in row_best[row]):  # if this node IS a row-best node however
                    break                                       # go to next column, as there will not be any more dominated nodes
                else:                                           # else
                    removed[row, col] = -np.inf                 # it is dominated and can be removed from any stable assignment
        
        return removed
    
    def _remove_row_dominated(self, col_best, row_ordering):
        """ Remove row-dominated nodes from a channel-matrix-copy.

        Parameters
        ----------
        col_best : list of lists
            The column-optimal nodes as in a list for each column.
        row_ordering : np.ndarray
            The row-ordering.
        """        
        removed = self.channels.copy()    # Create a return matrix
        
        for row in range(self.dims[0]):                     # in each column c: O(|C|)
            for col in reversed(row_ordering[row]):             # get the next-worst channel in this column
                if (self.channels[row, col] in col_best[col]):  # if this node IS a row-best node however
                    break                                       # go to next column, as there will not be any more dominated nodes
                else:                                           # else
                    removed[row, col] = -np.inf                 # it is dominated and can be removed from any stable assignment
        
        return removed

    def _get_ranks(self):
        """ Calculate the order of indices, which can sort the matrix
            either row-wise descending or column-wise descending.
            Moreover, calculates the ranking of each respective node/value, also
            either row-wise or column-wise (descending).

        Returns
        -------
        np.ndarray
            row_ranking: the individual row-ranking (descending) of nodes in given system.
            col_ranking: the individual column-ranking (descending) of nodes in given system.
            args_row: the order of indices of nodes, which can sort the matrix row-wise descending.
            args_col: the order of indices of nodes, which can sort the matrix column-wise descending.
        """        
        args_row = np.argsort(self.channels, axis=1)[:, ::-1]  # descending order argsort
        row_ranking = args_row.argsort(axis=1)
        args_col = np.argsort(self.channels, axis=0)[::-1]  # descending order argsort
        col_ranking = args_col.argsort(axis=0)
        return row_ranking, col_ranking, args_row, args_col

    def _get_row_opt(self, row_rankings):
        """ Retrieves the row-optimal nodes/values. (As a list of lists, because each row may have a different quota!)

        Parameters
        ----------
        row_rankings : np.ndarray
            The row rankings.

        Returns
        -------
        list of lists
            The row-optimal nodes values in a list for each row.
        """        
        row_best = []
        for r in range(self.dims[0]):
            row_best.append([])
            for c in range(self.dims[1]):
                if row_rankings[r, c] < self.max_columnplayers_per_row[r]:
                    row_best[r].append(self.channels[r,c])

        return row_best
    
    def _get_col_opt(self, column_rankings):
        """ Retrieve the column-optimal nodes/values in the variable column_best.  (As a list, because each row may have a different quota!)

        Parameters
        ----------
        column_rankings : np.ndarray
            The column rankings.

        Returns
        -------
        list of lists
            The column-optimal nodes values in a list for each column.
        """        
        column_best = []
        for c in range(self.dims[1]):
            column_best.append([])
            for r in range(self.dims[0]):
                if column_rankings[r, c] < self.max_rowplayers_per_column[c]:
                    column_best[c].append(self.channels[r, c])

        return column_best
    
def get_assignment_strategy(strategy: str, data: tuple[np.ndarray, int, int]) -> AssignmentProblemSolutionStrategy:
    """ Returns a new instance of a AssignmentProblemSolutionStrategy, based on the supplied strategy-choice. """
    strategies = {
        "all"   : ALL(data),
        "gsd"   : GSDR(data),
        "dom"   : DOM(data)
    }
    if strategy in strategies:
        return strategies[strategy]
    else:
        print(f"Unknown Assignment Strategy {strategy} (valid options: {strategies.keys()})")
        sys.exit()
        
def get_assignment(strategy: str, data: tuple[np.ndarray, int, int]) -> np.ndarray:
    return get_assignment_strategy(strategy, data).solve()
