from abc import ABC, abstractmethod
import multiprocessing as mp
import sys

import numpy as np
import cvxpy as cp

from utils.sca.SurrogateFunction import SumRateApprox, ProductRateApprox, SumEEApprox
from utils.sca.DataLoader import DataLoader
from utils.metrics import sumrate, prod_rate, sum_ee
from params import params


# global variables
noise = params["noise_power"]
pmax = params["pmax"]
rate_reqs = params["mean_rate_requirements"]

class Optimizer(ABC):
    def __init__(self, x_0:np.ndarray, data:DataLoader, 
                 tol:float=1e-3, verbose:bool=False, solver_verbose:bool=False):
        """
        Abstract base class for optimization algorithms.

        Parameters
        ----------
        x_0 : numpy.ndarray
            Initial power values for the users. 
            Shape: (num_ap, num_realizations, num_ue).
        data : DataLoader
            Data loader object that provides neccessary parameters.
        tol : float, optional
            Tolerance for convergence, by default 1e-3.
        verbose : bool, optional
            If True, prints optimization progress information, by default False.
        solver_verbose : bool, optional
            If True, prints solver-specific information, by default False.
        """
        self.x_0 = x_0
        self.data = data
        self.tol = tol
        self.verbose = verbose
        self.s_verbose = solver_verbose
        self.channels, self.num_ap, self.nr, self.num_ue, self.assignments = self.data.get_data()
        self.opt_power = cp.Variable((self.num_ap*self.num_ue))
        self.n = 0

    def optimize_realization(self, n, channels, x_0, **kwargs):
        """
        Optimization for a specific realization.

        Parameters
        ----------
        n : int
            The realization index.
        channels : numpy.ndarray
            The channel data for the given realization.
            Shape: (num_ap, num_ue).
        x_0 : numpy.ndarray
            Initial power values for the users.
            Shape: (num_ap, num_realizations, num_ue).
        **kwargs: dictionary
            Other keyword arguments.

        Returns
        -------
        np.ndarray
            The optimized power allocation for the given realization and user.
            Shape: (num_ap, num_ue).
        """
        solution = []
        self.n = n
        for p_i_n in x_0:
            tol_val = 1
            p_i = p_i_n[:,n,:]
            while(tol_val>self.tol):
                self.opt_power = cp.Variable((self.num_ap*self.num_ue), 
                                             nonneg=True)
                self.constraints = self.create_constraints(channels)
                self.objective = self.create_objective(p_i[:,None,:], 
                                                        channels)
                self.problem = cp.Problem(self.objective, self.constraints)
                try:
                    self.problem.solve(solver='MOSEK', verbose=self.s_verbose)
                except cp.error.SolverError:
                    print(f'error at {n}.')
                    break
                tol_val = self.stopping_criteria(channels,p_i[:,None,:])
                p_i = self.opt_power.value.reshape(p_i.shape)
                if self.verbose:
                    print(self.problem.status)
                    print(f'sum power per AP is {p_i.sum(-1)}')
            solution.append(p_i)
        best_sol = self.find_best(solution)
        return best_sol

    def parallel_opt(self, num_processes=None):
        """
        Optimize the power allocation for the given channel and user data 
        in parallel.

        This method parallelizes the optimization process for multiple 
        realizations using multiprocessing. It divides the realizations among 
        multiple processes to accelerate the optimization procedure.

        Parameters
        ----------
        num_processes : int, optional
            The number of parallel processes to use for optimization.
            If not provided, the number of processes will be set to the number 
            of available CPU cores.

        Returns
        -------
        numpy.ndarray
            A 3D array containing the optimized power allocation for 
            each realization and user.
            Shape: (num_ap, num_realizations, num_ue).
        """
        num_realizations = self.nr
        x_0 = self.x_0
        self.solution = []

        if num_realizations == 1:
            for n in range(num_realizations):
                self.n = n
                try:
                    channels = self.channels[:, n, :, :]
                except IndexError:
                    channels = []

                solution = self.optimize_realization(n, channels, x_0)
                self.solution.append(solution)

        else:
            if num_processes is None:
                num_processes = mp.cpu_count()

            pool = mp.Pool(num_processes)
            self.solution = pool.starmap(self.optimize_realization, 
                                         [(n, self.channels[:, n, :, :], x_0) 
                                           for n in range(num_realizations)])
            pool.close()
            pool.join()

        return np.stack(self.solution, axis=1)

    def optimize(self):
        """
        Optimize the power allocation for the given channel and user data.

        Returns
        -------
        numpy.ndarray
            A 3D array containing the optimized power allocation for 
            each realization and user. 
            Shape: (num_ap, num_realizations, num_ue ).
        """        
        L = self.num_ap
        K = self.num_ue
        num_realizations = self.nr
        x_0 = self.x_0
        self.solution = []
        for n in range(num_realizations):
            self.n = n
            try:
                channels = self.channels[:,n,:,:]
            except IndexError:
                channels = []
            
            power = self.optimize_realization(n,channels,x_0)
            self.solution.append(power)
            print(f'{self.n+1} out of {self.nr}')
        return np.stack(self.solution,axis=1)

    @abstractmethod
    def create_constraints(self, *args) ->list:
        """
        Abstract method for creating optimization constraints.

        Parameters
        ----------
        *args
            Variable-length argument list.

        Returns
        -------
        list
            A list of optimization constraints.
        """
        pass

    @abstractmethod
    def create_objective(self, *args) -> cp.Maximize | cp.Minimize:
        """
        Abstract method for creating the optimization objective.

        Parameters
        ----------
        *args
            Variable-length argument list.

        Returns
        -------
        cp.Maximize or cp.Minimize
            The optimization objective.
        """
        pass

    @abstractmethod
    def stopping_criteria(self, *args) -> float:
        """
        Abstract method for computing the stopping criteria.

        Parameters
        ----------
        *args
            Variable-length argument list.

        Returns
        -------
        float
            The stopping criteria value.
        """
        pass

    def find_best(self, solution:list):
        """
        Find the best solution from the list of solutions.

        Parameters
        ----------
        solution : list
            A list of power allocation solutions.

        Returns
        -------
        np.ndarray
            The best power allocation solution.
        """
        return solution[0]
    
    
class MinimizeSumPower(Optimizer):
    def __init__(self, data: DataLoader, solver: str='MOSEK', verbose: bool = False, solver_verbose: bool = False):
        super().__init__(None, data, None, verbose, solver_verbose)
        self.solver = solver
        
    #                            !!         *kwargs             *kwargs
    def create_constraints(self, n: int, coupling: np.ndarray, x: cp.Variable) -> list:
        rhs_eq = noise * np.ones(self.num_ue)
        constraints = [0 <= x, coupling @ x == rhs_eq]
            
        # Max-power constraints for each AP! (pmax defined globally)
        counter = 0
        ap_x_index_mapping = [ [] for l in range(self.num_ap)]
        for k in range(self.num_ue):
            for ap in range(self.num_ap):
                if self.assignments[n, ap, k] == 1:
                    ap_x_index_mapping[ap].append(counter)
                    counter += 1
        for ap in range(self.num_ap):
            constraints.append(cp.sum(x[ap_x_index_mapping[ap]]) <= pmax)
        
        return constraints
    
    #                           !!
    def create_objective(self, x: cp.Variable, *args) -> cp.Minimize:
        return cp.Minimize(cp.sum(x))
    
    def optimize(self) -> list:
        results = []
        status = []

        for n in range(self.nr):
            power, optstatus = self.optimize_realization(n)
            results.append(power)
            status.append(optstatus)
            
            if self.verbose and (((self.nr + (n + 1)) / self.nr) * 100) % 5 == 0: # Every 5%, print the progress
                # Progress Bar from:  https://stackoverflow.com/questions/3002085/how-to-print-out-status-bar-and-percentage
                sys.stdout.write('\r')
                sys.stdout.write(f"Status: Solved {self.nr + (n + 1)} of {self.nr} total optimization problems [{(self.nr + (n + 1)) / self.nr * 100}%]")
                sys.stdout.flush()

        return results, status
    
    #                               !!
    def optimize_realization(self, n: int) -> tuple:
        # Construct the coupling matrix
        coupling = self._construct_coupling(n)
        
        # Dimension of optimization variable
        x_dim = coupling.shape[1]
            
        # Define the optimization variable
        x = cp.Variable(x_dim)
        
        # Get the objective and constraints
        objective = self.create_objective(x)
        constraints = self.create_constraints(n, coupling, x)
        problem = cp.Problem(objective, constraints)
        
        # Call the algorithm to solve system of linear equations
        problem.solve(solver=self.solver, verbose=self.s_verbose)
        
        if problem.status != "optimal":
            return (0, problem.status)
        else:
            return (x.value, problem.status)
    
    
    def _construct_coupling(self, n: int):
        global noise        # Declare as global, so we can assign to this variable
        L = self.num_ap
        K = self.num_ue
        
        # Initialize Variables for coupling-procedure
        coupling = []
        ec_full = np.zeros((L, K, K))
        
        # -------------- Preparation for coupling-construction --------------
        for ap_index, ec_ap in enumerate(self.channels[:, n, :, :]):
            # for each AP
            ec_full[ap_index] = ec_ap  # (K,K) matrix
            
            # Negate betas (off-diagonals, i.e. interference)
            ec_full[ap_index][~np.eye(ec_full[ap_index].shape[0], dtype=bool)] *= -1
            
            # Apply the rate-requirements on alphas (i.e. 'signals')
            np.fill_diagonal(ec_full[ap_index], np.diag(ec_full[ap_index]) / rate_reqs)
                
        # -------------- Construction of coupling matrix --------------
        for k in range(K):
            for l in range(L):
                if self.assignments[n, l, k] == 1:
                    coupling.append(ec_full[l, k, :])
        coupling = np.asarray(coupling).T
        
        # Normalize by the noise power
        coupling /= noise
        noise = 1
        
        return coupling
    
    
class MaximizeEE(Optimizer):
    def __init__(self, x_0: np.ndarray, pc:float, data: DataLoader, 
                 tol: float = 0.001, verbose: bool = False):
        """
        Optimization class to maximize energy efficiency (EE).

        Parameters
        ----------
        x_0 : numpy.ndarray
            Initial power values for the users. 
            Shape: (num_ap, num_realizations, num_ue).
        pc : float
            The power consumption of the system's circuitry.
        data : DataLoader
            Data loader object that provides the neccessary parameters.
        tol : float, optional
            Tolerance for convergence, by default 0.001.
        verbose : bool, optional
            If True, prints optimization progress information, by default False.
        """
        super().__init__(x_0, data, tol, verbose)
        self.pc = pc
        self.approx = SumEEApprox(pc=self.pc)
        self.t = 0.01

    def create_constraints(self, channels=None) -> list:
        """
        Create optimization constraints.

        Parameters
        ----------
        channels : numpy.ndarray, optional
            The channel information for the users.

        Returns
        -------
        list
            A list of optimization constraints.
        """
        q = self.opt_power
        constraints = [q>=0]

        return constraints
    
    def create_objective(self, x_0=None, channels=None) -> cp.Maximize:
        """
        Create the optimization objective.

        Parameters
        ----------
        x_0 : numpy.ndarray, optional
            The initial power values for the users.

        channels : numpy.ndarray, optional
            The channel information for the users.

        Returns
        -------
        cp.Maximize
            The optimization objective.
        """
        q = self.opt_power
        self.approx.update_approximation(channels=channels, x_0=x_0, 
                                         opt=q, t=self.t)
        approx = self.approx.get_approximation()
        # self.update_t()
        objective = cp.Maximize(approx)

        return objective
    
    def update_t(self):
        """
        Update the parameter t for the optimization problem.

        Returns
        -------
        None
        """
        self.t = (self.approx.rate/self.approx.power_consumption).value
    
    def stopping_criteria(self, channels, p_i) -> float:
        """
        Compute the stopping criteria for the optimization.

        Parameters
        ----------
        channels : numpy.ndarray
            The channel information for the users.

        p_i : numpy.ndarray
            The current power values for the users.

        Returns
        -------
        float
            The stopping criteria value.
        """

        self.approx.update_approximation(channels=channels, 
                                         x_0=self.opt_power.value.reshape(p_i.shape), 
                                         opt=self.opt_power, t=self.t)
        see_pn = self.approx.get_approximation().value
        tol = see_pn.item() 
        self.update_t()
        
        return tol
    
    def find_best(self, solution: list):
        """
        Find the best solution from a list of power allocations based on 
        energy efficiency.

        Parameters
        ----------
        solution : list
            A list of power allocation arrays for different realizations. 
            Each array has shape (num_ap, num_ue).

        Returns
        -------
        numpy.ndarray
            The power allocation array that maximizes the energy efficiency.
        """
        ee = {i:sum_ee(self.num_ue, self.channels[:,self.n,:,:][:,None,:], 
                       power[:,None,:], self.pc, noise) 
              for i, power in enumerate(solution)}
        best_solution = solution[max(ee, key=ee.get)]
        return best_solution

class MaximizeSumRate(Optimizer):
    def __init__(self, x_0: np.ndarray, p_max:float, data: DataLoader, 
                 tol: float = 0.001, verbose: bool = False):
        """
        Optimization class to maximize the sum rate of the system.

        Parameters
        ----------
        x_0 : numpy.ndarray
            Initial power values for the users. 
            Shape: (num_ap, num_realizations, num_ue).
        p_max : float
            Maximum power constraint for each access point (AP).
        data : DataLoader
            Data loader object that provides channel information and parameters.
        tol : float, optional
            Tolerance for convergence, by default 0.001.
        verbose : bool, optional
            If True, prints optimization progress information, by default False.
        """
        super().__init__(x_0, data, tol, verbose)
        self.pmax = p_max
        self.rate_req = data.rate_req
        self.approx = SumRateApprox()

    def create_constraints(self,channels=None) -> list:
        """
        Create optimization constraints.

        Parameters
        ----------
        channels : numpy.ndarray, optional
            The channel information for the users.

        Returns
        -------
        list
            A list of optimization constraints.
        """
        q = self.opt_power
        constraints = [cp.sum(q[l*self.num_ue:(l+1)*self.num_ue])<=self.pmax 
                       for l in range(self.num_ap)]
        constraints.append(q>=0)

        # rate constraints 
        norm_channel = channels/noise
        temp = cp.vstack([cp.multiply(norm_channel[l],
                                      q[np.newaxis,l*self.num_ue:(l+1)*self.num_ue]) 
                                      for l in range(self.num_ap)])
        temp_chan = [temp[l*self.num_ue:(l+1)*self.num_ue,:] 
                    for l in range(self.num_ap)]
        alpha = [cp.diag(t) for t in temp_chan]
        beta =  [t-cp.diag(a) for t,a in zip(temp_chan, alpha)]
        a = cp.sum(alpha)
        b = cp.sum([cp.sum(_b, axis=1) for _b in beta]) + 1
        constraints.append(a>=cp.multiply(self.rate_req[self.n], b))
        return constraints
    
    def create_objective(self, x_0=None, channels=None) -> cp.Maximize:
        """
        Create the optimization objective.

        Parameters
        ----------
        x_0 : numpy.ndarray, optional
            The initial power values for the users.

        channels : numpy.ndarray, optional
            The channel information for the users.

        Returns
        -------
        cp.Maximize
            The optimization objective.
        """
        q = self.opt_power
        self.approx.update_approximation(channels=channels, x_0=x_0, opt=q)
        approx = self.approx.get_approximation()
        objective = cp.Maximize(cp.sum(approx))

        return objective
    
    def stopping_criteria(self, channels, p_i) -> float:
        """
        Compute the stopping criteria for the optimization.

        Parameters
        ----------
        channels : numpy.ndarray
            The channel information for the users.

        p_i : numpy.ndarray
            The current power values for the users.

        Returns
        -------
        float
            The stopping criteria value.
        """
        channels = channels[:,None,:,:]
        sr_pi = sumrate(self.num_ue, channels, p_i, noise)
        sr_pn = sumrate(self.num_ue, channels, 
                        self.opt_power.value.reshape(p_i.shape), noise)
        tol = sr_pn - sr_pi
        return tol

    def find_best(self, solution: list):
        """
        Find the best solution from a list of power allocations based on 
        sum rate.

        Parameters
        ----------
        solution : list
            A list of power allocation arrays for different realizations. 
            Each array has shape (num_ap, num_ue).

        Returns
        -------
        numpy.ndarray
            The power allocation array that maximizes the energy efficiency.
        """
        sr = {i:sumrate(self.num_ue, self.channels[:,self.n,:,:][:,None,:,:], 
                        power[:,None,:], noise) 
              for i, power in enumerate(solution)}
        best_solution = solution[max(sr, key=sr.get)]
        return best_solution
    
class MaximizeProductRate(MaximizeSumRate):
    def __init__(self, x_0: np.ndarray, p_max: float, data: DataLoader,
                  tol: float = 0.001, verbose: bool = False):
        """
        Optimization class to maximize the product rate of the system.

        Parameters
        ----------
        x_0 : numpy.ndarray
            Initial power values for the users. 
            Shape: (num_ap, num_realizations, num_ue).
        p_max : float
            Maximum power constraint for each access point (AP).
        data : DataLoader
            Data loader object that provides channel information and parameters.
        tol : float, optional
            Tolerance for convergence, by default 0.001.
        verbose : bool, optional
            If True, prints optimization progress information, by default False.
        """
        super().__init__(x_0, p_max, data, tol, verbose)
        self.approx = ProductRateApprox()

    def stopping_criteria(self, channels, p_i) -> float:
        """
        Compute the stopping criteria for the optimization.

        Parameters
        ----------
        channels : numpy.ndarray
            The channel information for the users.

        p_i : numpy.ndarray
            The current power values for the users.

        Returns
        -------
        float
            The stopping criteria value.
        """
        channels = channels[:,None,:,:]
        pr_pi = np.log(prod_rate(self.num_ue, channels, p_i, noise))
        pr_pn = np.log(prod_rate(self.num_ue, channels, 
                        self.opt_power.value.reshape(p_i.shape), noise))
        if self.verbose:
            print(f'previous:{pr_pi}, current: {pr_pn}')
        tol = pr_pn - pr_pi
        return tol
    
    def find_best(self, solution: list):
        """
        Find the best solution from a list of power allocations based on 
        log product rate.

        Parameters
        ----------
        solution : list
            A list of power allocation arrays for different realizations. 
            Each array has shape (num_ap, num_ue).

        Returns
        -------
        numpy.ndarray
            The power allocation array that maximizes the energy efficiency.
        """
        pr = {i:prod_rate(self.num_ue, self.channels[:,self.n,:,:][:,None,:,:], 
                          power[:,None,:], noise) 
              for i, power in enumerate(solution)}
        best_solution = solution[max(pr, key=pr.get)]
        return best_solution

# Optimizer Factory   
def create_optimizer(optimizer_type:str, **kwargs):
    """
    Create an optimizer based on the given type.

    Parameters
    ----------
    optimizer_type : str
        The type of optimizer to create. 
        Supported types: 
            'maximize_ee', 'maximize_sum_rate', 'maximize_product_rate'.

    x_0 : numpy.ndarray
        Initial power values for the users. 
        Shape: (num_ap, num_realizations, num_ue).

    data : DataLoader
        Data loader object that provides channel information and parameters.

    **kwargs
        Additional keyword arguments specific to each optimizer type.

    Returns
    -------
    Optimizer
        An instance of the chosen optimizer class.
    """
    optimizer_types = {
    'max_ee': MaximizeEE,
    'max_sr': MaximizeSumRate,
    'max_pr': MaximizeProductRate,
    }
    optimizer_class = optimizer_types.get(optimizer_type)
    if optimizer_class is None:
        raise ValueError(f"Unsupported optimizer type: {optimizer_type}. Please choose from {list(optimizer_types.keys())}.")

    return optimizer_class(**kwargs)