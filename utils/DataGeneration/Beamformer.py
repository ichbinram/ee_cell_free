from abc import ABC, abstractmethod
import sys

import numpy as np
import cvxpy as cp

from utils.helpers import normalize, normalize_rows, \
                        generate_random_complex_array, \
                        remove_row, remove_column, remove_zero_columns

class Beamformer(ABC):
    """ Abstract Base class for any Beamformer.
    """
    
    @abstractmethod
    def __init__(self, data: dict):
        """ Initialize a new Beamformer object

        Parameters
        ----------
        data : dict
            dictionaray consisting of the keys "complex_channels" and "assignments" data
        """        
        super().__init__()
        self.complex_channels = data["complex_channels"]
        self.ue_ap_assignment_matrix = data["assignments"]
        self.L = self.complex_channels.shape[0]
        self.n = self.complex_channels.shape[1]
        self.N = self.complex_channels.shape[2]
        self.K = self.complex_channels.shape[3]
        
        # Initialize Return Variables
        self.beamformer = np.zeros((self.n, self.L, self.K, self.N), dtype=np.cfloat)
        self.effective_channels = np.zeros((self.L, self.n, self.K, self.K), dtype=np.cfloat)
        
        # Mapping from ALL users in system to SERVED users per access point
        self.served_user_count = self._get_served_user_count()
        self.served_user_indices = self._get_served_user_indices()
        self.complex_channels_served = self._get_served_user_channel_matrix()
        self.beamformer_served = [ [ np.zeros((int(self.served_user_count[n, l]), self.N), dtype=np.cfloat) for l in range(self.L)] for n in range(self.n)] 
    
    @abstractmethod
    def _compute_bf_per_ap(self, n: int, l: int):
        """ Computes the respective beamforming matrix of shape (K, N) for a specific realization n and access point l and stores it
            in the instance variable beamformer.

        Parameters
        ----------
        n : int
            the realization number
        l : int
            the access point
        """        
    
    @abstractmethod
    def _apply_bf(self):
        """ Applies the beamformer matrix on the channels."""
        
    def _compute_bf(self):
        """ Computes the full beamforming matrix."""
        for n in range(self.n):
            for l in range(self.L):
                if self.served_user_count[n, l] > 0:
                    self._compute_bf_per_ap(n, l)
    
    def _compute_channelpower(self):
        """ Helper: Apply absolute and square operations on the channel, to get real values."""
        self.effective_channels = np.real(np.square(np.abs(self.effective_channels)))
        
        
    def _get_served_user_channel_matrix(self) -> list[list[type[np.ndarray]]]:
        """ Helper: Removes channels from the self.complex_channels, such that only channels to (potentially) served users remain.
            Potentially served users are defined in self.ue_ap_assignment_matrix.
            The shape changes from (L, n, N, K) -> (L, n, N, K_served). However, K_served may be different in each AP, such that the
            result can not be stored in a single numpy array, but in a list instead.

        Returns
        -------
        list
            the resulting served users channel matrices, contained in a list for each AP, for each realization.
        """        
        # Initialize the list
        complex_channels_served2 = [ [ np.ndarray for n in range(self.n)] for l in range(self.L)]
        
        # "Zero" out all unused channels / unserved users
        mapped_matching = np.expand_dims(self.ue_ap_assignment_matrix, axis=2)
        removed_channels = np.swapaxes(mapped_matching, 0, 1) * self.complex_channels
        
        # Iterate over n, l to remove zero-ed columns (unused channels / unserved users)
        for l in range(self.L):
            for n in range(self.n):
                complex_channels_served2[l][n] = remove_zero_columns(removed_channels[l, n])
                
        return complex_channels_served2
    
    def _get_served_user_indices(self) -> list[list[np.ndarray]]:
        """ Helper: Return the indices of all served users per realization, per AP.

        Returns
        -------
        list[list[np.ndarray]]
            the indices of served users for each realization and access point.
        """        
        served_user_indices = [ [ np.ndarray for l in range(self.L)] for n in range(self.n)]
        
        for n in range(self.n):
            for l in range(self.L):
                served_user_indices[n][l] = np.nonzero(self.ue_ap_assignment_matrix[n, l])[0]
                
        return served_user_indices
    
    def _get_served_user_count(self) -> np.ndarray:
        """Helper: Return the amount of served users per realization, per AP

        Returns
        -------
        np.ndarray
            the amount of served users per realization, per AP: shape (n, l).
        """
        count = np.sum(self.ue_ap_assignment_matrix, axis=2)
        return count
    
    def _map_to_served_beamformer(self, n: int, l: int):
        """ Helper: Maps the computed beamformer matrix to served dimension by removing rows for unserved users.

        Parameters
        ----------
        n : int
            the realization number
        l : int
            the access point
        """        
        counter = 0
        
        for k in range(self.K):
            # If this user is being served..
            if k in self.served_user_indices[n][l]:
                # .. then take over it's computed beamformer
                self.beamformer_served[n][l][counter] = self.beamformer[n, l, k, :]
                counter += 1
    
    def _remapping_to_system(self, n: int, l: int):
        """ Helper: Remaps the served beamformer matrix to original dimensions by filling in 0s for unserved users.

        Parameters
        ----------
        n : int
            the realization number
        l : int
            the access point
        """
        
        counter = 0
        # Iterate over all users in the system
        for k in range(self.K):
            # If this user is being served ...
            if k in self.served_user_indices[n][l]:
                # ... then take over it's computed beamformer
                self.beamformer[n, l, k, :] = self.beamformer_served[n][l][counter]
                counter += 1
                # ... else keep the zero-rows from initialization
    
    ###########################    
    ######### Getter ##########
    ###########################
    def _get_beamformer_served(self) -> list[list[type[np.ndarray]]]:
        """ Helper: Retrieve the beamformer matrices, only for served users. 

        Returns
        -------
        list[list[type[np.ndarray]]]
            the served user beamformer matrix.
        """        
        self._compute_bf()
        return self.beamformer_served
    
    def get_beamformer(self) -> np.ndarray:
        """ Retrieve the Beamforming matrix.

        Returns
        -------
        np.ndarray
            the beamforming matrix
        """        
        self._compute_bf()
        return self.beamformer
    
    def get_effective_channels(self) -> np.ndarray:
        """ Retrieve the effective channels after applying the Beamforming matrices.

        Returns
        -------
        np.ndarray
            the effective channels
        """        
        self._compute_bf()
        self._apply_bf()
        self._compute_channelpower()
        return self.effective_channels
    

class DigitalBeamformer(Beamformer):
    """ Abstract class for digital Beamformers."""
    
    @abstractmethod
    def __init__(self, data: dict):
        """ Initialize a new DigitalBeamformer object.

        Parameters
        ----------
        data : dict
            dictionaray consisting of the keys "complex_channels" and "assignments" data
        """        
        super().__init__(data)
        
    @abstractmethod
    def _compute_bf_per_ap(self, n: int, l: int):
        """ Compute the respective beamforming matrix for a specific realization n and access point l and stores it
            in the instance variable beamformer.

        Parameters
        ----------
        n : int
            the realization number
        l : int
            the access point
        """        
            
    def _apply_bf(self):
        """ Applies the beamformer matrix on the channels."""
        for n in range (self.n):
                for l in range(self.L):
                    self.effective_channels[l, n] = (self.beamformer[n, l] @ self.complex_channels[l, n]).T

    
class NewZFBeamformer(DigitalBeamformer):
    """ Zero-Forcing Digital Beamformer."""
    
    def __init__(self, data: dict):
        """ Initialize a new ZFBeamformer object

        Parameters
        ----------
        data : dict
            dictionaray consisting of the keys "complex_channels" and "assignments" data
        """        
        super().__init__(data)
    
    def _compute_bf_per_ap(self, n: int, l: int):
        """ Compute the "Zero-Forcing" beamforming matrix for a specific realization n and access point l and stores it
            in the instance variable beamformer_served.

        Parameters
        ----------
        n : int
            the realization number
        l : int
            the access point
        """
        bf = np.linalg.pinv(self.complex_channels[l][n])
        self.beamformer[n][l] = normalize_rows(bf)
        self._map_to_served_beamformer(n, l)


class ZFBeamformer(DigitalBeamformer):
    """ Zero-Forcing Digital Beamformer."""
    
    def __init__(self, data: dict):
        """ Initialize a new ZFBeamformer object

        Parameters
        ----------
        data : dict
            dictionaray consisting of the keys "complex_channels" and "assignments" data
        """        
        super().__init__(data)
    
    def _compute_bf_per_ap(self, n: int, l: int):
        """ Compute the "Zero-Forcing" beamforming matrix for a specific realization n and access point l and stores it
            in the instance variable beamformer_served.

        Parameters
        ----------
        n : int
            the realization number
        l : int
            the access point
        """
        bf = np.linalg.pinv(self.complex_channels_served[l][n])
        self.beamformer_served[n][l] = normalize_rows(bf)
        self._remapping_to_system(n, l)                

                
class MRTBeamformer(DigitalBeamformer):
    """ Maximum-Ratio-Transmission Digital Beamformer."""
    
    def __init__(self, data: dict):
        """ Initialize a new MRTBeamformer object

        Parameters
        ----------
        data : dict
            dictionaray consisting of the keys "complex_channels" and "assignments" data
        """        
        super().__init__(data)
        
    def _compute_bf_per_ap(self, n: int, l: int):
        """ Compute the "Maximum-Ratio-Transmission" beamforming matrix for a specific realization n and access point l and stores it
            in the instance variable beamformer_served.

        Parameters
        ----------
        n : int
            the realization number
        l : int
            the access point
        """        
        bf_served = np.transpose(np.conj(self.complex_channels_served[l][n]))
        self.beamformer_served[n][l] = normalize_rows(bf_served)
        self._remapping_to_system(n, l)
        
            
class PZFBeamformer(DigitalBeamformer):
    """ Partial Zero-Forcing Digital Beamformer."""
    
    def __init__(self, data: dict):
        """ Initialize a new PZFBeamformer object

        Parameters
        ----------
        data : dict
            dictionaray consisting of the keys "complex_channels" and "assignments" data
        """        
        super().__init__(data)
        self.zf_selection = self._get_selection_by_threshold()
        
    def _compute_bf_per_ap(self, n: int, l: int):
        """ Compute the "Partial Zero-Forcing" beamforming matrix, based on an interference-threshold value for
            a specific realization n and access point l and store it in the instance variable beamformer.

        Parameters
        ----------
        n : int
            the realization number
        l : int
            the access point
        """        
        for k in range(self.K):
            if np.all(self.zf_selection[n, l, k, :] == 0):               # If for this user there are 'no' interfering users
                bf = np.transpose(np.conj(self.complex_channels[l, n, :, k]))  # Compute BF-Vector
                self.beamformer[n, l, k, :] = normalize(bf)
            
            else:                                                               # else
                #-------------- Get the selected user's channels for Zeroforcing -----------------------------------
                #                     [1, K] *                            [N, K]
                A = self.zf_selection[n, l, k, :] * self.complex_channels[l, n, :, :]   # Apply the selection on the channels, to zero out non-zfed users.
                A = A[~np.all(A==0, axis=1)]                                       # remove the non-zfed users -> shape (N, J)
                
                #-------------- Projector to orthogonal complement-----------------------------------
                projector = self._get_proj_to_orth_compl(A)
                
                #-------------- Beamforming vector for user k-----------------------------------
                channel_k = self.complex_channels[l, n, :, k].T   # receive the current user k channel
                
                bf = projector @ channel_k                       # compute the bf-vector
                bf = normalize_rows(bf)
                self.beamformer[n, l, k, :] = np.conj(bf)        # !!! Need to conjugate the term, to get results equal to regular ZF !!!
    
    def _get_selection_by_threshold(self, interference_threshold:float=1.8e-5) -> np.ndarray:
        """ Compute the ZF-selection matrix by comparing each channel-gain with the input interference_threshold value.

        Parameters
        ----------
        interference_threshold : float, optional
            the wanted interference threshold value, by default 1.8e-5

        Returns
        -------
        np.ndarray
            the resulting zf-selection matrix
        """        
        # partial ZF: Choose users to be zf-ed, based on a threshold value
        zf_selection = np.zeros((self.n, self.L, self.K, self.K))
        for n in range(self.n):
            for l in range(self.L):
                for k in range(self.K):
                    if self.ue_ap_assignment_matrix[n, l, k]:                                           # Only for assigned users k
                        # ------- Beamforming MRT, to help estimating interference between users ------------------------
                        mrt = np.transpose(np.conj(self.complex_channels[l, n, :, k]))  # Compute BF-Vector
                        mrt = normalize(mrt)
                        
                        for j in range(self.K):
                            if j != k:                                                                  # For all other users j
                                interference_k_j = mrt @ self.complex_channels[l, n, :, j]              # Compute interference from k to j
                                #print(f"Interference from {k} to {j}: [abs={np.abs(interference_k_j)}]")
                                # 
                                if np.abs(interference_k_j) > interference_threshold:                   # check, if interference exceeds some threshold value
                                    zf_selection[n, l, k, j] = 1                                        # if yes, this user will be ZF-ed away
        
        return zf_selection
    
    def _get_proj_to_orth_compl(self, A):
        """ Computes and returns the 'projector to the orthogonal complement of space' spanned by the input matrix A.

        Parameters
        ----------
        A : np.ndarray
            The input matrix of shape (N, J).

        Returns
        -------
        np.ndarray
            The orthogonal projection of A of shape (N, N).
        """        
        I = np.identity(A.shape[0])     # (N, N)
        A_herm = np.conjugate(A.T)
        
        AhA = A_herm @ A
        if np.linalg.cond(AhA) < 1/np.finfo(AhA.dtype).eps:         # Check condition for regular inverse
            inv = np.linalg.inv(AhA)
        else:                                                       # otherwise perform pseudo-inverse
            inv = np.linalg.pinv(AhA)

        projector = I - A @ inv @ A_herm
        return projector

class HybridBeamformer(Beamformer):
    """ Abstract class for digital Beamformers."""
    
    @abstractmethod
    def __init__(self, data: dict):
        """ Initialize a new HybridBeamformer object

        Parameters
        ----------
        data : dict
            dictionaray consisting of the keys "complex_channels" and "assignments" data
        """        
        super().__init__(data)
        self.num_rfchains = 2
        
        # Beamformers with dimension for served users
        self.benchmark_bf = MRTBeamformer(data)._get_beamformer_served()
        self.digital_bf_served = [ [ np.zeros((int(self.served_user_count[n, l]), self.num_rfchains), dtype=np.cfloat) for l in range(self.L)] for n in range(self.n)]
        
        # Complete system Beamformers
        self.digital_bf = np.zeros((self.n, self.L, self.K, self.num_rfchains), dtype=np.cfloat)
        self.analog_bf = np.zeros((self.n, self.L, self.num_rfchains, self.N), dtype=np.cfloat)
        
        
    @abstractmethod
    def _compute_bf_per_ap(self, n: int, l: int):
        """ Compute the digital and analog beamforming matrices for a specific realization n and access point l and store them
            in respective instance variables.

        Parameters
        ----------
        n : int
            given realization number
        l : int
            given access point
        """        
        
    def _apply_bf(self):
        """ Apply the beamformer matrix on the channels."""
        for n in range (self.n):
            for l in range(self.L):
                self.effective_channels[l, n] = (self.digital_bf[n, l] @ self.analog_bf[n, l] @ self.complex_channels[l, n]).T
    
    def _remapping_to_system(self, n: int, l: int):
        """ Helper: Remaps the served beamformer matrices to original dimensions by filling in 0s for unserved users.

        Parameters
        ----------
        n : int
            given realization number
        l : int
            given access point
        """        
        counter = 0
        
        # Iterate over all users in the system
        for k in range(self.K):
            # If this user is being served ...
            if k in self.served_user_indices[n][l]:
                # ... then take over it's computed beamformer
                self.digital_bf[n, l, k, :] = self.digital_bf_served[n][l][counter]
                counter += 1
                # ... else keep the zero-rows from initialization
        self.beamformer[n, l] = self.digital_bf[n, l] @ self.analog_bf[n, l]
        self.beamformer[n, l] = normalize_rows(self.beamformer[n, l])
                

class HDAMBeamformer(HybridBeamformer):
    """ 'Hybrid Design by Alternating Minimization' Hybrid Beamformer
        Algorithm 1 from Heath et al. 'Low Complexity Hybrid Precoding Strategies for Millimeter Wave Communication Systems'
        
        Constraint: K == self.num_rfchains
    """
    def __init__(self, data: dict):
        """ Initialize a new HDAMBeamformer object

        Parameters
        ----------
        data : dict
            dictionaray consisting of the keys "complex_channels" and "assignments" data
        """        
        super().__init__(data)
        self.iters = 15
        
    
    def _compute_bf_per_ap(self, n: int, l: int):
        """ Compute the digital and analog beamforming matrices (by HDAM) for a specific realization n and access point l and store them
            in respective instance variables.

        Parameters
        ----------
        n : int
            given realization number
        l : int
            given access point
        """        
        
        # Introduce temp variables for readability (transpose, because algorithms use transposed dimensions)
        bbf = self.benchmark_bf[n][l].T
        dbf = self.digital_bf_served[n][l].T
        abf = self.analog_bf[n, l].T
        
        # 1) Initialize the analog beamformer by frf = fopt / |fopt|
        abf = np.divide(bbf, np.abs(bbf), out=np.zeros_like(bbf, dtype=np.cfloat), where=np.abs(bbf)!=0)
        
        
        # 2) Do self.iters amount of alternating update-iterations
        for iter in range(self.iters):
            # 2.a) Update the digital beamformer (Orthonormal Procrustes Problem)
            u, _, vt = np.linalg.svd(np.conj(bbf).T @ abf)
            dbf = np.conj(vt).T @ np.conj(u).T
            
            # 2.b) Update the analog beamformer
            abf = bbf @ np.conj(dbf).T
            abf = np.divide(abf, np.abs(abf), out=np.zeros_like(abf), where=np.abs(abf)!=0)

        # 2.c) OPTIONAL (for better performance): Give up unitary structure of digital beamformer and update by least squares
        dbf = np.linalg.inv(np.conj(abf).T @ abf) @ np.conj(abf).T @ bbf

        # 3) Normalize the digital beamformer
        dbf = np.sqrt(self.K) * (1 / np.linalg.norm(abf @ dbf, ord='fro')) * dbf
        
        # Finally update the instance variables (dont forget to transpose back!)
        self.analog_bf[n, l] = abf.T
        self.digital_bf_served[n][l] = dbf.T
        self.beamformer_served[n][l] = self.digital_bf_served[n][l] @ self.analog_bf[n, l]
        self.beamformer_served[n][l] = normalize_rows(self.beamformer_served[n][l])
        
        # Re-mapping to original dimension (K_served -> K)
        self._remapping_to_system(n, l)
        

class HDCVXRBeamformer(HybridBeamformer):
    """ 'Hybrid Design by Convex Relaxation' Hybrid Beamformer
        Algorithm 3 from Heath et al. 'Low Complexity Hybrid Precoding Strategies for Millimeter Wave Communication Systems'
        
        Constraint: K <= self.num_rfchains
    """
    
    def __init__(self, data: dict):
        """ Initialize a new HDCVXRBeamformer object

        Parameters
        ----------
        data : dict
            dictionaray consisting of the keys "complex_channels" and "assignments" data
        """        
        super().__init__(data)
        self.solver = cp.MOSEK
        self.beta = 0.05 # 0 < beta << 1 , size of trust region
        self.iters = 6
        
    def _compute_bf_per_ap(self, n: int, l: int):
        """ Compute the digital and analog beamforming matrices (by HDCVXR) for a specific realization n and access point l and store them
            in respective instance variables.

        Parameters
        ----------
        n : int
            given realization number
        l : int
            given access point
        """        
        # Introduce temp variables for readability
        bbf = self.benchmark_bf[n][l].T
        dbf = self.digital_bf_served[n][l].T
        abf = self.analog_bf[n, l].T
        
        # 1.a) Initialize the digital and analog beamformer with random values (zeros lead to NO convergence!! Values always stay zero)
        dbf = generate_random_complex_array(dbf.shape)
        abf = generate_random_complex_array(abf.shape)
        
        # Progress Bar from:  https://stackoverflow.com/questions/3002085/how-to-print-out-status-bar-and-percentage
        sys.stdout.write('\r')
        sys.stdout.write(f"Status: Computing HBF for realization {n} and AP {l}")
        sys.stdout.flush()
        
        # 1.b) Do self.iters amount of update-iterations
        for iter in range(self.iters):
            
            # 2) Update the analog beamformer (column-wise, for all columns j in num_rfchains):
            for j in range(self.num_rfchains):
                abf_no_j = remove_column(abf, j)
                dbf_no_j = remove_row(dbf, j)
                
                # 2.a) Solve optimization problem
                rj = bbf - abf_no_j @ dbf_no_j
                x = cp.Variable((self.N, 1), complex=True)
                f = rj - x @ np.expand_dims(dbf[j], axis=0)
                objective = cp.Minimize(cp.square(cp.norm(f, p='fro')))
                constraints = [cp.abs(x) - 1 <= self.beta]
                problem = cp.Problem(objective=objective, constraints=constraints)
                problem.solve(solver=self.solver, verbose=False)
                
                # Catch the case, when the optimization problem could not be solved
                if problem.status not in ['optimal', 'optimal_inaccurate']:
                    raise RuntimeWarning(f"Could not solve HDCVXR optimization problem (Realization {n}, AP {l})! Status: *{problem.status}* ")
                
                # 2.b) Update j-th column of the analog beamformer:
                abf[:, j] = normalize(x.value.squeeze())
            
            # 3) Update the digital beamformer completely:
            dbf = np.linalg.pinv(np.conj(abf).T @ abf) @ np.conj(abf).T @ bbf
            
        # 4) Normalize the digital beamformer
        dbf = np.sqrt(self.K) * np.where(np.linalg.norm(abf @ dbf, ord='fro') != 0, 1 / np.linalg.norm(abf @ dbf, ord='fro'), 0) * dbf
        
        # Finally update the instance variables
        self.analog_bf[n, l] = abf.T
        self.digital_bf_served[n][l] = dbf.T
        self.beamformer_served[n][l] = normalize_rows(self.digital_bf_served[n][l] @ self.analog_bf[n, l])
        
        # Re-mapping to original dimension (K_served -> K)
        self._remapping_to_system(n, l)
            

""" 
def tmmse(self, tol=1e-3):
        
        effective_channels = np.zeros(self.dims_out, dtype=np.cfloat)
        for n in range(self.n):
            # Initialize powers; shape (L,K)
            p_next = np.ones(self.K)
            
            # ------------------ TMMSE approach ------------------
            iteration = 0
            sum_powers = [np.sum(p_next)]
            diff = [100]
            
            while diff[iteration] > tol:
                # Initialization: The precoding/beamforming vector for [n]
                omega = np.zeros((self.L, self.K, self.N), dtype=np.cfloat)
                omega_normalized = np.zeros((self.L, self.K, self.N), dtype=np.cfloat)
                for l in range(self.L): 
                    # Intermediate computations: Compute V_l
                    diag = np.diag(p_next)
                    term1 = self.complex_channels[l, n] @ diag @ np.conjugate(self.complex_channels[l, n].T) + np.identity(self.N)
                    term1_inv = np.linalg.inv(term1)
                    V_l = term1_inv @ self.complex_channels[l,n] @ sp.sqrtm(diag)
                    
                    # Finally, compute the beamformer omega for AP l
                    for k in range(self.K):
                        omega[l, k, :] = V_l @ np.identity(self.K)[k]
                        omega_normalized[l, k, :] = normalize(omega[l, k, :])
                        
                    effective_channels[l, n] = (omega_normalized[l] @ self.complex_channels[l, n]).T       # Apply the BF-normalized matrix on the channels
                
                
                # Compute SINR (UL) values for users
                rss_ul_k = np.zeros(self.K)
                if_k = np.zeros(self.K)
                # 1) Gather RSS for every user
                for k in range(self.K):
                    rss_ul_k[k] = p_next[k] * compute_channelpower(np.trace(herm(self.complex_channels[:, n, :, k]) @ omega_normalized[:, :, k]))
                    for j in range(self.K):
                        if j != k:
                            if_k[k] += p_next[j] * compute_channelpower(np.trace(herm(self.complex_channels[:, n, :, j]) @ omega_normalized[:, :, k]))
                # 2) Compute SINR values
                sinr_k = rss_ul_k / ("noise??" + if_k)
                
                
                # Compute the powers for the next iteration
                p_next = p_next / sinr_k.T
                
                # Check Sum-Powers for convergence
                sum_powers.append(np.sum(p_next))
                print(f"Iteration: {iteration}\n Sum-Powers: {sum_powers[iteration]}\n sinr: {sinr_k}\n")
                
                # Update Iteration
                iteration += 1
                diff.append(np.abs(sum_powers[iteration-1] - sum_powers[iteration]))
                if iteration == 50:
                    break
                
        return effective_channels
                

"""