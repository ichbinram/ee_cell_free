from abc import ABC, abstractmethod

import numpy as np

from utils.DataGeneration.Beamformer import *

class BeamformerFactory(ABC):
    """ Factory for the Beamformer-class.
        The factory does not maintain any of the instances it creates."""
    
    @abstractmethod
    def create_beamformer(self, data: tuple[np.ndarray, np.ndarray]) -> Beamformer:
        """ Create and return a new Beamformer instance.

        Parameters
        ----------
        data : tuple[np.ndarray, np.ndarray]
            tuple consisting of (complex_channels, ue_ap_selection) data

        Returns
        -------
        Beamformer
            the newly created Beamformer object
        """        
        
class NewZFBeamformerFactory(BeamformerFactory):
    """ Factory aimed at providing the ZF-Beamformer."""
    
    def create_beamformer(self, data: tuple[np.ndarray, np.ndarray]) -> Beamformer:
        """ Return a new ZFBeamformer instance.

        Parameters
        ----------
        data : tuple[np.ndarray, np.ndarray]
            tuple consisting of (complex_channels, ue_ap_selection) data

        Returns
        -------
        Beamformer
            the newly created ZFBeamformer object
        """        
        return NewZFBeamformer(data)
    
class ZFBeamformerFactory(BeamformerFactory):
    """ Factory aimed at providing the ZF-Beamformer."""
    
    def create_beamformer(self, data: tuple[np.ndarray, np.ndarray]) -> Beamformer:
        """ Return a new ZFBeamformer instance.

        Parameters
        ----------
        data : tuple[np.ndarray, np.ndarray]
            tuple consisting of (complex_channels, ue_ap_selection) data

        Returns
        -------
        Beamformer
            the newly created ZFBeamformer object
        """        
        return ZFBeamformer(data)

class MRTBeamformerFactory(BeamformerFactory):
    """ Factory aimed at providing the ZF-Beamformer."""
    
    def create_beamformer(self, data: tuple[np.ndarray, np.ndarray]) -> Beamformer:
        """ Return a new MRTBeamformer instance.

        Parameters
        ----------
        data : tuple[np.ndarray, np.ndarray]
            tuple consisting of (complex_channels, ue_ap_selection) data

        Returns
        -------
        Beamformer
            the newly created MRTBeamformer object
        """        
        return MRTBeamformer(data)

class PZFBeamformerFactory(BeamformerFactory):
    """ Factory aimed at providing the ZF-Beamformer."""
    
    def create_beamformer(self, data: tuple[np.ndarray, np.ndarray]) -> Beamformer:
        """ Return a new PZFBeamformer instance.

        Parameters
        ----------
        data : tuple[np.ndarray, np.ndarray]
            tuple consisting of (complex_channels, ue_ap_selection) data

        Returns
        -------
        Beamformer
            the newly created PZFBeamformer object
        """        
        return PZFBeamformer(data)
    
class HDCVXRBeamformerFactory(BeamformerFactory):
    """ Factory aimed at providing the HDCVXR-Beamformer."""
    
    def create_beamformer(self, data: tuple[np.ndarray, np.ndarray]) -> Beamformer:
        """ Return a new HDCVXRBeamformer instance.

        Parameters
        ----------
        data : tuple[np.ndarray, np.ndarray]
            tuple consisting of (complex_channels, ue_ap_selection) data

        Returns
        -------
        Beamformer
            the newly created HDCVXRBeamformer object
        """        
        return HDCVXRBeamformer(data)

class HDAMBeamformerFactory(BeamformerFactory):
    """ Factory aimed at providing the HDAM-Beamformer."""
    
    def create_beamformer(self, data: tuple[np.ndarray, np.ndarray]) -> Beamformer:
        """ Return a new HDAMBeamformer instance.

        Parameters
        ----------
        data : tuple[np.ndarray, np.ndarray]
            tuple consisting of (complex_channels, ue_ap_selection) data

        Returns
        -------
        Beamformer
            the newly created HDAMBeamformer object
        """        
        return HDAMBeamformer(data)

def get_factory(choice: str) -> BeamformerFactory:
    """ Returns a new instance of a BeamformerFactory, based on the supplied choice.

    Parameters
    ----------
    choice : str
        The beamformer choice ("mrt", "zf", "pzf", "hdam" or "hdcvxr")

    Returns
    -------
    BeamformerFactory
        The respective BeamformerFactory-object
    """    
    factories = {
        "mrt"       : MRTBeamformerFactory(),
        "nzf"       : NewZFBeamformerFactory(),
        "zf"        : ZFBeamformerFactory(),
        "pzf"       : PZFBeamformerFactory(),
        "hdcvxr"    : HDCVXRBeamformerFactory(),
        "hdam"      : HDAMBeamformerFactory()
    }
    if choice in factories:
        return factories[choice]
    else:
        raise ValueError(f"Unknown Beamformer {choice} (valid options: {factories.keys()})")