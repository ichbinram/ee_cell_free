import time
import multiprocessing as mp

import numpy as np
import torch
from torch.utils.data import DataLoader

from utils.ml.gnn import GNNGlobal
from utils.ml.core import sum_ee
from utils.ml.core import CFData as CFDataML
from utils.Optimization.Optimizer import create_optimizer
from utils.Optimization.DataLoader import NpzDataLoader, CFDataLoader
from utils.DataGeneration.CFData import CFData
from utils.metrics import sum_ee as sca_ee
from params import params

if __name__ == "__main__":
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    file_path = 'data/chunks_10ue_5ap_5ant'
    model_path = 'results/24-07-2023_14-21-11/model_37000'
    # data = NpzDataLoader(file_path)
    params["num_data_samples"] = 20
    datagen = CFData(params)
    # datagen.save_as_chunks(128)
    data = CFDataLoader(datagen)
    params["mean_channel"] = datagen.effective_channels.mean()
    params["std_channel"] = datagen.effective_channels.std()
    channels = data.effective_channels

    # pmax = params["pmax"]
    x_0 = np.random.rand(1, data.num_ap, data.num_realizations, 
                        data.num_ue)
    t_sca0 = time.time()
    mp.freeze_support()
    optimizer = create_optimizer('max_ee', x_0=x_0, data=data, pc=4.)
    power_ee = optimizer.parallel_opt()
    sumee = sca_ee(data.num_ue, channels, power_ee, 4., params["noise_power"])
    t_sca1 = time.time()
    t_sca = t_sca1 - t_sca0

    t_ml0 = time.time()
    ml_ee = GNNGlobal(params, device= device)
    ml_ee.load_state_dict(torch.load(model_path, 
                                     map_location=torch.device(device)))
    ml_ee.eval()
    ee_ml = []
    dataset = CFDataML(params, path=file_path, device=device)
    test_data = DataLoader(dataset, batch_size=params["batch_size"])
    for indices, channels, required_rates, selection in test_data:
        power, a, b, support = ml_ee((channels - params["mean_channel"]) / 
                                            params["std_channel"])
        ee_ml.append(sum_ee(channels, power, params).mean())
    ee_ml = torch.stack(ee_ml)
    t_ml1 = time.time()
    t_ml = t_ml1 - t_ml0

    print(f'SCA:{t_sca}s, GNN:{t_ml}s')
    print(f'EE: SCA:{sumee.mean()}, GNN:{ee_ml.mean()}')
