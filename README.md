# Energy-Efficient Power Allocation in Cell-Free Massive MIMO Networks via Graph Neural Networks
This code package is related to the following scientific article:

Ramprasad Raghunath, Bile Peng, Eduard Jorwieck, " Energy-Efficient Power Allocation in Cell-Free Massive MIMO Networks via Graph Neural Networks "
submitted to IEEE International Conference on Machine learning in Communications and Networking.


## Getting started

It is a good idea to generate the data before you start training or testing the model.
You can find a data generation script under scripts/gen_channels_in_chunks.py.
To train the model:
1. change the corresponding parameters in the 'params.py' file.
2. generate training and test data using 'gen_channels_in_chunks.py'.
    - please note the naming convention for the data path. Example: Testing: 'data/chunks_10ue_5ap_5ant_testing', Training: 'data/chunks_10ue_5ap_5ant'
3. run scripts/train_v2.py with the following flags: (--device, --record, --num_data_samples, [--num_chunks or --dataset_path])
4. run scripts/sca.py to test the trained model with SCA. (please update the model location in the script)

The models would be stored in the results folder.

