from abc import ABC, abstractmethod
from dataclasses import dataclass

import numpy as np

from utils.DataGeneration.CFData import CFData


@dataclass
class DataLoader(ABC):
    def __post_init__(self):
        """
        Abstract base class for data loaders.

        This class is intended to be used as a base class for data loaders
        that provide channel information and other data for optimization algorithms.
        """
        self.effective_channels = None 
        self.num_ap = None 
        self.num_realizations = None
        self.num_ue = None
        self.rate_req = None
        self.ue_ap_assignments = None
        self.set_data()

    @abstractmethod
    def set_data(self): 
        """
        Abstract method to set the data for the optimization algorithm.

        Implementations of this method should set the appropriate values for 
        effective_channels, num_ap, num_realizations, num_ue, and rate_req.

        Returns
        -------
        None
        """       
        pass

    def get_data(self):
        """
        Get the data for the optimization algorithm.

        Returns
        -------
        list
            A list containing the effective_channels, num_ap, num_realizations, and num_ue.
        """
        return [self.effective_channels, self.num_ap, 
                self.num_realizations, self.num_ue, self.ue_ap_assignments]
    

@dataclass
class CFDataLoader(DataLoader):
    """
    Data loader class for Cell-free (CF) data.

    This class extends the base DataLoader class and is intended to be used 
    specifically for loading data from the CFDataGenerator. 
    It sets the necessary data attributes based on the information provided by 
    a CFDataGenerator object.

    Parameters
    ----------
    cfdata : CFDataGenerator
        An instance of the CFDataGenerator class that provides the CF data.

    Attributes
    ----------
    cfdata : CFDataGenerator
        An instance of the CFDataGenerator class.

    effective_channels : numpy.ndarray
        Array containing the effective channel information.

    num_ap : int
        The number of access points (APs).

    num_ue : int
        The number of user equipment (UE) or users.

    num_realizations : int
        The number of data samples.

    rate_req : numpy.ndarray
        Array containing the rate requirements for each data sample.
    """
    cfdata:CFData

    def set_data(self):
        """
        Set the data necessary for the optimization algorithm.

        Overrides the set_data method in the base DataLoader class.

        Returns
        -------
        None
        """
        self.effective_channels = self.cfdata.effective_channels
        self.num_ap = self.cfdata.L
        self.num_ue = self.cfdata.K
        self.num_realizations = self.cfdata.num_realizations
        self.rate_req = self.cfdata.rate_requirements
        self.ue_ap_assignments = self.cfdata.assignment_adjacency_mat

@dataclass
class NpzDataLoader(DataLoader): 
    """
    Data loader class for NumPy NPZ files.

    This class extends the base DataLoader class and is intended to be used 
    specifically for loading data from NumPy NPZ files. It sets the necessary 
    data attributes based on the information provided in the NPZ file.

    Parameters
    ----------
    file_path : str
        The file path to the NumPy NPZ file.

    Attributes
    ----------
    file_path : str
        The file path to the NumPy NPZ file.

    file : numpy.lib.npyio.NpzFile
        The NumPy NPZ file object.

    effective_channels : numpy.ndarray
        Array containing the effective channel information.

    num_ap : int
        The number of access points (APs).

    num_ue : int
        The number of user equipment (UE) or users.

    num_realizations : int
        The number of channel realizations.

    rate_req : numpy.ndarray
        Array containing the rate requirements for each realization.
    """
    file_path:str
    def __post_init__(self):
        """
        Initialize the NpzDataLoader object.

        Overrides the __post_init__ method in the base DataLoader class.

        Returns
        -------
        None
        """
        self.file = np.load(self.file_path)
        self.set_data()
        del self.file
    
    def set_data(self):
        """
        Set the data from the NPZ file for the optimization algorithm.

        Overrides the set_data method in the base DataLoader class.

        Returns
        -------
        None
        """
        self.effective_channels = self.file.f.channels
        self.rate_req = self.file.f.rr 
        self.num_ap = self.effective_channels.shape[0]
        self.num_ue = self.effective_channels.shape[-1]
        self.num_realizations = self.rate_req.shape[0]
              