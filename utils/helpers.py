# External libraries
import numpy as np


#####################################################
##---------------- Unit Conversion ----------------##
#####################################################
def db2pow(r):
    """ Converts the dB value given in the argument to the corresponding Power value such that 10*log10(r) = r_dB.
    """
    return 10 ** (r / 10)


#####################################################
##---------------- Matrix Creation ----------------##
#####################################################
def generate_random_complex_array(shape: tuple[int]) -> np.ndarray:
    """ Helper: Generate and return a new numpy array containing complex values in the provided dimensions.

    Parameters
    ----------
    shape : tuple[int]
        shape of wanted array

    Returns
    -------
    np.ndarray
        the resulting complex array
    """    
    real = np.random.uniform(-1, 1, size=shape) 
    imag = np.random.uniform(-1, 1, size=shape)
    return real + 1j * imag


#####################################################
##--------------- Matrix Operations ---------------##
#####################################################
def normalize(array: np.ndarray) -> np.ndarray:
    """ Helper: Normalize a given input array.

    Parameters
    ----------
    array : np.ndarray
        the input array

    Returns
    -------
    np.ndarray
        the resulting array

    Raises
    ------
    TypeError
        If the input dimensions are more than 2.
    """    
    if array.ndim == 1:  # if input is a vector
        norm = np.sqrt(np.sum(np.abs(array)**2))                            # norm of vec
    elif array.ndim == 2: # else input is mat
        norm = np.sqrt(np.sum((np.abs(array) ** 2), axis=1, keepdims=True)) # norm of Matrix
    else: 
        raise TypeError("Input argument not supported!")
    return np.divide(array, norm, out=np.zeros_like(array), where=norm!=0)  # Normalization step

def normalize_rows(array: np.ndarray) -> np.ndarray:
    """ Helper: Normalize a given 2-dimensional array row-wise.

    Parameters
    ----------
    array : np.ndarray
        the input matrix

    Returns
    -------
    np.ndarray
        the resulting matrix
    """    
    assert array.ndim == 2, f"Expected 2-dim array, got a {array.ndim}-dim array!"
    norm = np.linalg.norm(array, axis=1)
    return np.divide(array.T, norm, out=np.zeros_like(array.T), where=norm!=0).T

def remove_row(matrix: np.ndarray, j: int) -> np.ndarray:
    """ Helper: Remove a row j from a 2-dimensional matrix and returns the resulting matrix.

    Parameters
    ----------
    matrix : np.ndarray
        the input matrix
    j : int
        the row to remove

    Returns
    -------
    np.ndarray
        the resulting matrix
    """    
    
    assert matrix.ndim == 2, f"Dimensions unclear: Expected matrix.ndim={2}, but got matrix.ndim={matrix.dim}!"
    return np.delete(matrix, j, axis=0)

def remove_column(matrix: np.ndarray, j: int) -> np.ndarray:
    """ Helper: Remove a column j from a 2-dimensional matrix and returns the resulting matrix.

    Parameters
    ----------
    matrix : np.ndarray
        the input matrix
    j : int
        the column to remove

    Returns
    -------
    np.ndarray
        the resulting matrix
    """    
    assert matrix.ndim == 2, f"Dimensions unclear: Expected matrix.ndim={2}, but got matrix.ndim={matrix.dim}!"
    return np.delete(matrix, j, axis=1)

def remove_zero_columns(matrix: np.ndarray) -> np.ndarray:
    """ Helper: Remove all zero-columns from a 2-dimensional matrix and returns the resulting matrix.

    Parameters
    ----------
    matrix : np.ndarray
        the input matrix

    Returns
    -------
    np.ndarray
        the resulting matrix
    """    
    assert matrix.ndim == 2, f"Dimensions unclear: Expected matrix.ndim={2}, but got matrix.ndim={matrix.dim}!"
    return matrix[:, np.any(matrix != 0, axis=0)]

def remove_zero_rows(matrix: np.ndarray) -> np.ndarray:
    """ Helper: Remove all zero-rows from a 2-dimensional matrix and returns the resulting matrix.

    Parameters
    ----------
    matrix : np.ndarray
        the input matrix

    Returns
    -------
    np.ndarray
        the resulting matrix
    """    
    assert matrix.ndim == 2, f"Dimensions unclear: Expected matrix.ndim={2}, but got matrix.ndim={matrix.dim}!"
    return matrix[np.any(matrix != 0, axis=1), :]

#####################################################
##--------------------- Other ---------------------##
#####################################################
def create_bit_sequences(n: int) -> list:
    """ Return all possible bit sequences of length n.

    Parameters
    ----------
    n : int
        length of the bit sequences.

    Returns
    -------
    list
        all possible bit sequences of length n.
    """    
    
    bit_sequences = []
    for i in range(2 ** n):
        bit_sequence = [int(bit) for bit in bin(i)[2:].zfill(n)]
        bit_sequences.append(bit_sequence)
        
    return bit_sequences

def reshape_bit_sequences(seqs: list, L:int, K:int) -> list:
    """ Reshape bit-sequences of length L*K, given in argument seqs,
        into matrices of shape (L, K).

    Parameters
    ----------
    seqs : list
        list of bit-sequences of length L*K.
    L : int
        dimension 0 of the output matrices.
    K : int
        dimension 1 of the output matrices.

    Returns
    -------
    list
        list of matrices with shape (L, K).
    """    
    arrays = []
    for el in seqs:
        arrays.append(np.reshape(el, (K, L)))
    return arrays