import os
import torch
from torch.utils.data import Dataset
import torch.nn.functional as F
import numpy as np

def sinr(channel, power, params):
    """
    Compute the Signal-to-Interference-plus-Noise Ratio (SINR) for each user.

    Parameters
    ----------
    channel : torch.Tensor
        A tensor containing the channel gain between antennas and users.
        Shape: (num_ap, num_realizations, num_ue, num_ue).

    power : torch.Tensor
        A tensor containing the power allocation for each user.
        Shape: (num_ap, num_realizations, num_ue).

    params : dict
        A dictionary containing the parameters for computation.

    Returns
    -------
    torch.Tensor
        A tensor containing the SINR values for each user.
        Shape: (num_realizations, num_ue).
    """
    power = power[:, :, :, None, :]
    rss = (channel * power).sum(dim=[1, 2])
    sinr = [rss[:, user_idx, user_idx] / 
                       (torch.sum(rss[:, user_idx, :], dim=1)\
                         - rss[:, user_idx, user_idx] + params["noise_power"]) 
                         for user_idx in range(params["num_users"])]
    return torch.stack(sinr, dim=-1)

def sumrate(channel, power, params):
    """
    Compute the sum rate achieved by all users in the network.

    Parameters
    ----------
    channel : torch.Tensor
        A tensor containing the channel gain between antennas and users.
        Shape: (num_ap, num_realizations, num_ue, num_ue).

    power : torch.Tensor
        A tensor containing the power allocation for each user.
        Shape: (num_ap, num_realizations, num_ue ).

    params : dict
        A dictionary containing the parameters computation.

    Returns
    -------
    torch.Tensor
        A tensor containing the sum rate achieved by all users.
        Shape: (num_realizations,).
    """
    sinr_user = sinr(channel, power, params)
    rate = torch.log2(1+sinr_user)
    sum_rate = rate.sum(dim=-1)
    return sum_rate

def prodrate(channel, power, params):
    """
    Compute the product rate achieved by all users in the network.

    Parameters
    ----------
    channel : torch.Tensor
        A tensor containing the channel gain between antennas and users.
        Shape: (num_ap, num_realizations, num_ue, num_ue).

    power : torch.Tensor
        A tensor containing the power allocation for each user.
        Shape: (num_ap,  num_realizations, num_ue).

    params : dict
        A dictionary containing the parameters for computation.

    Returns
    -------
    torch.Tensor
        A tensor containing the product rate achieved by all users.
        Shape: (num_realizations, ).
    """
    sinr_user = sinr(channel, power, params)
    rate = torch.log2(1+sinr_user)
    prod_rate = rate.prod(dim=-1)
    return prod_rate

def sum_ee(channel, power, params):
    """
    Compute the sum of Energy Efficiency (EE) achieved by all users in 
    the network.

    Parameters
    ----------
    channel : torch.Tensor
        A tensor containing the channel gain between antennas and users.
        Shape: (num_ap, num_realizations, num_ue, num_ue).

    power : torch.Tensor
        A tensor containing the power allocation for each user.
        Shape: (num_ap, num_realizations, num_ue).

    params : dict
        A dictionary containing the parameters for computation.

    Returns
    -------
    torch.Tensor
        A tensor containing the sum of EE achieved by all users.
        Shape: (num_realizations, ).
    """
    pc = 4
    sinr_user = sinr(channel,power, params)
    rate = torch.log2(1+sinr_user)
    if isinstance(power,list):
        power_consumtion = torch.stack(power,dim=3).sum(dim=[1,2])
    else:
        power_consumtion = power.sum(dim=[1,2])
    ee = rate/(pc+power_consumtion)
    return ee.sum(dim=-1)

def determine_kappa(support, previous_kappa):
    """
    Determine the kappa value used in the loss function.

    Parameters
    ----------
    support : torch.Tensor
        Support of the distribution created from the reparametrization trick.

    previous_kappa : torch.Tensor
        The kappa value from the previous iteration.

    Returns
    -------
    torch.Tensor
        The updated kappa value for the current iteration.
    """
    lr_kappa = 1e-3
    if len(support) < 10:
        return previous_kappa
    elif support[-1] >= np.mean(support):
        return previous_kappa + lr_kappa
    else:
        return F.relu(previous_kappa - lr_kappa / 2)
    

class CFData(Dataset):
    """
    Custom PyTorch dataset for loading Cell-free data.

    This dataset class is designed to load CF data and provide samples for 
    training or testing machine learning models.

    Parameters
    ----------
    params : dict
        A dictionary containing the parameters for the dataset.

    path : str, optional
        The path to the directory where the CF data is stored, by default 
        an empty string.

    device : str, optional
        The device to load the data (e.g., 'cpu' or 'cuda'), by default 'cpu'.

    test : bool, optional
        If True, the dataset is used for testing and the number of samples per 
        chunk is limited to 128,
        by default False.

    Attributes
    ----------
    params : dict
        A dictionary containing the parameters for the dataset.

    test : bool
        If True, the dataset is used for testing.

    path : str
        The path to the directory where the CF data is stored.

    device : str
        The device to load the data (e.g., 'cpu' or 'cuda').

    samples_per_chunk : int
        The number of samples per chunk.

    total_chunks : int
        The total number of chunks in the dataset.

    chunk_num : int
        The current chunk number.

    counter : int
        The counter to keep track of the current sample number within the 
        current chunk.

    channels : torch.Tensor
        A tensor containing the effective channel information.

    rate_requirements : torch.Tensor
        A tensor containing the rate requirements for each sample.

    selection_matrices : torch.Tensor
        A tensor containing the selection matrices for each sample.

    sparse_channels : torch.Tensor
        A tensor containing the effective channel information with values below 
        the threshold zeroed out (for testing).
    """
    def __init__(self, params, path="", device="cpu", test=False):
        super().__init__()
        self.params = params
        self.test = test
        self.path = path
        self.device = device
        self.samples_per_chunk = self.params["num_samples_chunks"]
        self.total_chunks = int(np.ceil(self.params["num_data_samples"] / 
                                        self.samples_per_chunk))
        self.chunk_num = 0
        self.counter = 0
        self.channels, self.rate_requirements, self.selection_matrices = self.load_chunk(self.chunk_num, self.device,
                                                                                         self.path)

        self.channels = torch.stack(self.channels, dim=2)
        self.selection_matrices = torch.stack(self.selection_matrices, dim=2)
        self.to_sparse()

    def to_sparse(self):
        """
        Convert the effective channel information to a sparse representation 
        (for testing).

        If the dataset is used for testing, this method sets the sparse_channels 
        attribute, where values below the threshold 
        (params["ign_th"]) are set to zero.

        Returns
        -------
        None
        """
        if self.test:
            self.channels = self.channels[:128, :, :, :, :]
            zero_positions = self.channels > self.params["ign_th"]
            self.sparse_channels = self.channels * zero_positions
        else:
            zero_positions = self.channels > self.params["ign_th"]
            self.channels = self.channels * zero_positions

    def __getitem__(self, item):
        """
        Get a sample from the dataset.

        Parameters
        ----------
        item : int
            The index of the sample to retrieve.

        Returns
        -------
        tuple
            A tuple containing the sample information based on the dataset's test flag.
            If the dataset is used for testing, the tuple contains:
                (sample_index, effective_channel, sparse_channel, 0, selection_matrix)
            If the dataset is used for training, the tuple contains:
                (sample_index, effective_channel, 0, selection_matrix)
        """
        item = int(item % self.samples_per_chunk)
        self.counter += 1
        if self.counter > self.samples_per_chunk:
            self.chunk_num = (self.chunk_num + 1) % self.total_chunks  # iterate over the number of chunks
            self.channels, self.rate_requirements, self.selection_matrices = self.load_chunk(self.chunk_num,
                                                                                             self.device,
                                                                                             path=self.path)

            self.channels = torch.stack(self.channels, dim=2)
            self.selection_matrices = torch.stack(self.selection_matrices, dim=2)
            self.to_sparse()

        if self.test:
            return (item, self.channels[item, :, :, :, :],
                    self.sparse_channels[item, :, :, :, :],
                    0,
                    self.selection_matrices[item, :, :, :])
        else:
            return (item, self.channels[item, :, :, :, :],
                    0,
                    self.selection_matrices[item, :, :, :])

    def __len__(self):
        """
        Get the total number of samples in the dataset.

        Returns
        -------
        int
            The total number of samples in the dataset.
        """
        return self.params["num_data_samples"]

    def preselect(self, channel):
        """
        Preselect the antennas with the highest signal power for each user.

        Parameters
        ----------
        channel : torch.Tensor
            A tensor containing the effective channel information for a single sample.
            Shape: (num_realizations, num_ap, num_ue, num_ue).

        Returns
        -------
        torch.Tensor
            A tensor containing the selection matrix for preselected antennas.
            Shape: (num_ue, preselection_ap).
        """
        signal_channel = torch.diag(channel)
        _, indices = torch.topk(signal_channel, self.params["preselection_ap"])
        selection_mat = np.zeros((self.params["num_users"], 
                                  self.params["preselection_ap"]), 
                                  dtype=np.float32)
        for i, index in enumerate(indices):
            selection_mat[index, i] = 1
        selection_mat = torch.tensor(selection_mat)

        return selection_mat

    def load_chunk(self, chunk_num, device, path=""):
        """ Loads a previously saved chunk to memory.

        Args:
            chunk_num (int): The chunk id (integer) to be restored.
            path (str, optional): Optional. The path, where the chunks are stored.

        Returns:
            List, Tensor, List: Channels as a list with L items and each item 
            of the shape n x VirtualUsers x VirtualUsers, 
            rate requirement per user, 
            selection matrix per AP with same configuration as channels.
        """

        # -------------------- Initialize locals -------------------- #
        file_path = os.path.join(path, f'chunk_{chunk_num}.npz')

        # -------------------- load chunks to memory ---------------- #
        file = np.load(file_path)
        channels, rate_req, selection = file.f.channels, file.f.rr, file.f.sel
        channels = channels.astype("float32")
        rate_req = rate_req.astype("float32")
        selection = selection.astype("float32")

        channels = torch.from_numpy(channels).to(device).unbind()
        rate_req = torch.from_numpy(rate_req).to(device).unbind()
        selection = torch.from_numpy(selection).to(device).unbind()

        channels = [c[:, None, :, :] for c in channels]
        selection = [m[:, None, :, :] for m in selection]  # To enable broadcast
        self.counter = 0  # reset the counter for number of items sampled

        return channels, rate_req, selection