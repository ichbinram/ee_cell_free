# Standard libraries
from pathlib import Path
import pickle
import os
import time

# External libraries
import numpy as np

# Custom libraries
import utils.DataGeneration.helpers as util
from utils.helpers import db2pow
from utils.DataGeneration.UserToApAssignment import get_assignment
from utils.DataGeneration.BeamformerFactory import get_factory


class CFData():
    """ Class, which generates, modifies and contains random
        (real-world) data for a Cell-Free communications system,
        based on the provided parameters.
    """    
    ###########################################################################
    ##-------------------- Class initialization functions--------------------##
    ###########################################################################
    def __init__(self, params, verbose=False):
        # ---------------- Initialize necessary configuration data from provided params ----------------
        super().__init__()
        verboseprint = print if verbose else lambda *a, **k: None
        self.L = params["num_aps"]
        self.N = params["num_antennas"]
        self.K = params["num_users"]
        self.square_length = params["square_length"]
        self.num_realizations = params["num_data_samples"]
        self.max_users_per_ap = params["num_max_ues_per_ap"]   # number of users each AP can serve
        self.min_aps_per_user = params["num_min_aps_per_ue"]        # minimum number of APs per user
        self.selection_procedure = params["preselection"]
        self.beamformer = params["beamformer"]
        self.ap_locations = None
        self.ue_locations = None
            
        # ---------------- Generate new data ----------------
        self.rate_requirements = np.random.rand(self.num_realizations, self.K) * 0.0
        time_1 = time.time()
        self.channel_gains = self.gen_channel_data()
        
        time_2 = time.time()
        verboseprint(f"{'Channels were generated in:':<30} {time_2-time_1:>3} seconds")
        self.assignment_adjacency_mat = self.assign_ue_to_ap()
        self.selection_matrices = self.map_selection_to_virtualusers()
        
        time_3 = time.time()
        verboseprint(f"{'Users were assigned in:':<30} {time_3-time_2:>3} seconds")
        self.complex_channels = self.gen_complex_channels()
        
        time_4 = time.time()
        verboseprint(f"{'Complex channels generated in:':<30} {time_4-time_3:>3} seconds")
        self.effective_channels = self.apply_beamformer()
        
        time_5 = time.time()
        verboseprint(f"{'Beamforming was applied in:':<30} {time_5-time_4:>3} seconds")
        
    #####################################################################################################
    ##-------------------- data generation procedure - separated in steps/functions--------------------##
    #####################################################################################################
    def gen_channel_data(self):
        """ Generate the CF-model, based on the given configuration, and return the computed channel gains.

        Returns
        -------
        np.ndarray
            shape (num_realizations x L x K). The per-realization channel_gain-matrices are of shape (L x K).
        """        
        AP_pos = util.gen_ap_locations(self.L, self.square_length)
        self.ap_locations = AP_pos
        self.ue_locations = list()
        channel_gains = np.zeros((self.num_realizations, self.L, self.K))
            
        for n in range(self.num_realizations):
            # ----------------- Random data generation -----------------
            UE_pos = util.gen_ue_locations(self.K, self.square_length)
            self.ue_locations.append(UE_pos)
            ap_ue_dist = util.compute_dist(AP_pos, UE_pos)
            channel_gains[n] = util.gains(ap_ue_dist)
        
        return channel_gains

    def assign_ue_to_ap(self):
        """ Handles the assignment of users to APs, based on the given/wanted procedure.
            Necessary condition: The previously computed channel gains are available in class field 'channel_gains'.

        Returns
        -------
        np.ndarray
            shape (num_realizations x L x K).   The per-realization selected channels in form of an adjacency matrix are of shape (L x K).
                                                Values are either 0, for non-assigned channels, or 1, for assigned channels.
        """        
        assignment_adj_mat = []
        quota_ue = [self.min_aps_per_user] * self.K  # max amount of APs per UE
        quota_ap = [self.max_users_per_ap] * self.L  # max amount of UEs per AP
        
        for n in range(self.num_realizations):
            # -------------------- Get user selection -------------------- #
            adj = get_assignment(self.selection_procedure, (self.channel_gains[n].T, quota_ue, quota_ap))
            assignment_adj_mat.append(adj.T)
        
        return np.stack(assignment_adj_mat)
    
    def gen_complex_channels(self):
        """ Generate the complex channels.

        Returns
        -------
        np.ndarray
            shape (L x num_realizations x N x K).   Each c-channel matrix of shape (N x K) belongs to one AP (l) and a realization (n).
        """        
        R = np.eye(self.N)
        complex_channels = np.zeros((self.L, self.num_realizations, self.N, self.K), dtype=np.cfloat)
        
        for n in range (self.num_realizations):
            for l in range(self.L):
                
                # small-scale fading per AP l: from each AP antenna N_i, to each user k
                ss_fading = np.random.randn(self.N, self.K) + 1j * np.random.randn(self.N, self.K)
                for k in range(self.K):
                    spatial_R = db2pow(self.channel_gains[n, l, k]) * R
                    R_sqrt = np.sqrt(spatial_R, dtype=np.cfloat)
                    
                    complex_channels[l,n,:,k] = np.sqrt(0.5) * R_sqrt @ ss_fading[:, k]
        
        return complex_channels
    
    def apply_beamformer(self):
        """ Apply beamforming on the complex channels.

        Returns
        -------
        np.ndarray
            shape (L x num_realizations x K x K).   The effective system channels.
        """
        data = {"complex_channels": self.complex_channels, "assignments": self.assignment_adjacency_mat}
        
        factory = get_factory(self.beamformer)
        bf = factory.create_beamformer(data)
        return bf.get_effective_channels()
    
    #####################################################################################################
    ##-------------------- Helpers to easily change one algorithm in the procedure --------------------##
    #####################################################################################################
    def change_assignment(self, selection_procedure, matching_matrix=None):
        """ Sets a new matching of users to APs. The matching can either be provided to this function or
            if there is None provided, it will be computed.

        Parameters
        ----------
        selection_procedure : str
            The wanted matching procedure ("all", "gsd", "dom").
        matching_matrix : np.ndarray
            An already existing matching matrix.
        """        
        self.selection_procedure = selection_procedure
        if matching_matrix == None:
            self.assignment_adjacency_mat = self.assign_ue_to_ap()
        else:
            if matching_matrix.shape == (self.num_realizations, self.L, self.K):
                self.assignment_adjacency_mat = matching_matrix
            else:
                raise AssertionError(f"Input matching matrix has bad shape {matching_matrix.shape} ! Expected shape: {(self.num_realizations, self.L, self.K)}")
        self.selection_matrices = self.map_selection_to_virtualusers()
        
    def change_beamformer(self, beamformer):
        """ Computes the effective channels for a given Beamformer.

        Parameters
        ----------
        beamformer : str
            The wanted beamforming procedure ("mrt", "zf", "pzf")
        """        
        self.beamformer = beamformer
        data = {"complex_channels": self.complex_channels, "assignments": self.assignment_adjacency_mat}
        
        factory = get_factory(self.beamformer)
        bf = factory.create_beamformer(data)
        
        self.effective_channels = bf.get_effective_channels()

    #########################################################################################################
    ##-------------------- Save/Load an instance to/from disk (to preserve random data)--------------------##
    #########################################################################################################
    def save(self, path=""):
        """ Saves this CFData-instance to a file. The path to the storing directoy can either be given as an argument, or the instance will be stored by default in
            'data/[]ue_[]ap_[]ant/', where [] are filled with the current configuration, respectively.

        Parameters
        ----------
        path : str, optional
            The path of the storing directory, by default "".
        """        
        if path == "":
            path = "data/{k}ue_{l}ap_{n}ant/".format(k=self.K, l=self.L, n=self.N)
        Path(path).mkdir(parents=True, exist_ok=True)
        with open(path + "CFData_Instance.pkl", 'wb+') as outp:  # Creates if not existing & Overwrites any existing file.
            pickle.dump(self, outp, pickle.HIGHEST_PROTOCOL)

    @staticmethod
    def load(datapath):
        """ Loads a previously saved/stored instance of the CFData and returns the instance. This is a static method for the CFData-class. 

        Parameters
        ----------
        datapath : str
            The filepath, at which the instance has been stored.

        Returns
        -------
        CFDataGenerator
            The instance, which has been loaded.
        """        
        with open(datapath, 'rb') as input:
            loaded_instance = pickle.load(input)
        return loaded_instance


    #####################################################################################################
    ##-------------------- Helper functions necessary for torch/NN implementations --------------------##
    #####################################################################################################
    def map_selection_to_virtualusers(self):
        """ Helper function: Generates a 'different' selection matrix, where regular-user-indices as rows are mapped to 'virtual-user-indices' in columns, for each AP separately.

        Returns
        -------
        np.ndarray
            shape (L x num_realizations x K x K),                   if assignment "all" is specified (full information),
            shape (L x num_realizations x K x max_users_per_ap),    if the assigment is dependant on the maximum number of users an AP may serve.
        """        
        # Initialize the "Virtual-index-user to Standard-index-user" (K x K)-matrix for AP l, realization n
        if  self.selection_procedure == "all":
            selection_mat = np.zeros((self.L, self.num_realizations, self.K, self.K))
        else:
            selection_mat = np.zeros((self.L, self.num_realizations, self.K, self.max_users_per_ap))
            
        # -------------------- Map selected user indices to "virtual user" indices -------------------- #
        for n in range(self.num_realizations):
            ap_to_ue = np.nonzero(self.assignment_adjacency_mat[n])
            for l in range(self.L):
                # List of served users by AP l
                users_per_ap = ap_to_ue[1][ap_to_ue[0] == l]
                                
                # Mapping of users (index k) to virtual-users (index iter)
                for iter, k in enumerate(users_per_ap):
                    selection_mat[l, n, k, iter] = 1
        
        return selection_mat
    
    def save_as_chunks(self, chunksize: int, path=""):
        """ Saves the effective channels, rate requirements and selection matrices in smaller chunks, specifically chunks of the provided argument 'chunksize', to the disk.
            The path can be provided, the default path is configuration-dependant 'data/chunks_{k}ue_{l}ap_{n}ant/'.

        Parameters
        ----------
        chunksize : int
            The size (integer) of the chunks to be stored
        path : str, optional
            The path to the directory, where to save the data chunks, by default ""
        """        
        # -------------------- Create saving directory -------------------- #
        if path == "":
            path = "data/chunks_{k}ue_{l}ap_{n}ant/".format(k=self.K, l=self.L, n=self.N)
        Path(path).mkdir(parents=True, exist_ok=True)
        
        # -------------------- Initialize locals -------------------- #
        number_of_chunks = int(np.ceil(self.num_realizations / chunksize))
        
        # -------------------- Save each chunk separately to disk -------------------- #
        for chunk_i in range(number_of_chunks):
            appos_chunk_i = self.ap_locations[chunk_i*chunksize:chunksize*(chunk_i+1)]
            uepos_chunk_i = self.ue_locations[chunk_i*chunksize:chunksize*(chunk_i+1)]
            gains_chunk_i = self.channel_gains[chunk_i*chunksize:chunksize*(chunk_i+1)]
            rate_req_chunk_i = self.rate_requirements[chunk_i*chunksize:chunksize*(chunk_i+1)]
            channel_chunk_i = self.effective_channels[:,chunk_i*chunksize:chunksize*(chunk_i+1),:,:]
            selection_chunk_i = self.selection_matrices[:,chunk_i*chunksize:chunksize*(chunk_i+1),:,:]
            
            filename = f"chunk_{chunk_i}.npz"
            file_path = os.path.join(path, filename)
            np.savez(file_path, appos = appos_chunk_i, uepos = uepos_chunk_i, gains = gains_chunk_i, rr = rate_req_chunk_i, channels = channel_chunk_i, sel = selection_chunk_i)

    @staticmethod        
    def load_chunk_num(chunk_num: int, dir_path: str, cfdata=None):
        """ Loads a previously saved chunk to memory.

        Parameters
        ----------
        chunk_num : int
            The chunk id (integer) to be restored
        dir_path : str
            The path to the directory, where the chunks are stored

        Returns
        -------
        np.ndarray
            channel-matrices, rate_requirements, selection-matrices, appos, uepos, gains for the given chunk_num
        """        
        # -------------------- Initialize locals -------------------- #
        file_path = os.path.join(dir_path, f'chunk_{chunk_num}.npz')

        # -------------------- load chunks to memory ---------------- #
        file = np.load(file_path)
        channels, rate_req, selection = file.f.channels, file.f.rr, file.f.sel
        appos, uepos, gains = file.f.appos, file.f.uepos, file.f.gains
        if cfdata is not None:
            cfdata.effective_channels = channels
            cfdata.selection_matrices = selection
            cfdata.rate_requirements = rate_req

        return channels, rate_req, selection, appos, uepos, gains

    #############################################################
    ##-------------------- class to string --------------------##
    #############################################################
    def __str__(self):
        ret = "\nThis is an instance of the CFDataGenerator-Class!\nScenario configurations:\n"
        ret = ret + "---------------------------------------\n"
        ret = ret + "UEs: {k}\n".format(k=self.K)
        ret = ret + "APs: {k}\n".format(k=self.L)
        ret = ret + "Antennas per AP: {n}\n".format(n=self.N)
        ret = ret + "Square length: {k}\n".format(k=self.square_length)
        ret = ret + "Number of scenarios: {k}\n".format(k=self.num_realizations)
        ret = ret + "Max # UEs per AP: {k}\n".format(k=self.max_users_per_ap)
        ret = ret + "Min # APs per UE: {k}\n".format(k=self.min_aps_per_user)
        ret = ret + "Matching algorithm: {k}\n".format(k=self.selection_procedure)
        ret = ret + "Beamformer: {k}\n".format(k=self.beamformer)
        ret = ret + "---------------------------------------\n"
        return ret
    
    
    