import sys
import os
sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), ".."))
import torch
import numpy as np
import scipy.linalg as sp
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset
from utils.DataGeneration.UserToApAssignment import AssignmentProblem
from utils.DataGeneration.CFData import CFData
from params import params

# from graph_model import gen_locations,compute_dist,gains

alpha = 3.76  # Pathloss exponent
sigma_sf = 10  # Standard deviation of Shadow fading
constant_term = -35.3  # Average channel gain (in dB) at reference distance of 1m [->corresponds to -148.1 dB at 1km, with alpha=3.76]


def db2pow(r):
    """Converts the dB value given in the argument to the corresponding Power value such that 10*log10(r) = r_dB."""
    return 10 ** (r / 10)


def gen_locations(num, square_length=params["square_length"]):
    """Generate number of random points inside a square of given length on a cartesian plane.

    Args:
        num (_type_): number of points to be generated.
        square_length (int, optional): Length of the square in meters. Defaults to 1000.

    Returns:
        _type_: A matrix of dimension (num,2), where each row represents a point and column 0 represents the x coordinate and column 1 represents y coordinate 
    """
    loc = torch.zeros((num, 2)).uniform_() * square_length
    return loc
    # return np.random.default_rng().uniform(high=square_length, size=(num, 2))


def gen_grid_locations(num, square_length=params["square_length"]):
    # num_x = 5
    # try:
    #     if num < num_x:
    #         raise AssertionError('Less than 5 APs are not supported!')
    # except ValueError as err:
    #     print(err.args)
    #     sys.exit(1)

    num_x = int(np.ceil(np.sqrt(num)))
    num_y = int(np.ceil(num / num_x))
    x = np.linspace(0.05, 0.95, num_x) * square_length
    y = np.linspace(0.05, 0.95, num_y) * square_length
    x, y = np.meshgrid(x, y)
    x = np.reshape(x, (-1, 1))[:num, :]
    y = np.reshape(y, (-1, 1))[:num, :]
    return torch.tensor(np.concatenate((x, y), axis=1), dtype=torch.float32)


def gains(dist):
    """Compute the large scale fading based on distances between UE and AP.

    Args:
        dist (_type_): Matrix containing distance between UE and AP.

    Returns:
        _type_: A matrix containing the large scale fading between each UE and AP.
    """
    # return constant_term - alpha*10*np.log10(dist) + sigma_sf * np.random.randn(*dist.shape)
    gains = torch.minimum(torch.tensor(-80),
                          constant_term - alpha * 10 * torch.log10(dist) + sigma_sf * torch.randn(*dist.shape))
    return gains


def compute_dist(pos1, pos2):
    """A function to compute the distance between points on a cartesian plane.

    Args:
        pos1 (_type_): Coordinates of point 1 (x,y)
        pos2 (_type_): Coordinates of point 2 (x,y)

    Returns:
        _type_: Array containing distances between points.
    """
    dist = torch.cdist(pos1, pos2)
    dist = torch.sqrt(100 + dist ** 2)
    # dist = np.zeros((pos1.shape[0], pos2.shape[0]))
    # for l in range(pos1.shape[0]):
    #     for k in range(pos2.shape[0]):
    #         temp = (pos2[k][0] - pos1[l][0])**2 + (pos2[k][1] - pos1[l][1])**2
    #         dist[l, k] = np.sqrt(temp)

    return dist


# Message passing of one edge
class Phi1D(nn.Module):
    def __init__(self, params, input_dim, device="cpu"):
        super().__init__()
        self.phi1_local = nn.Conv1d(input_dim, params["phi_local_feature_dim"], 1).to(device)
        self.phi1_global = nn.Conv1d(input_dim, params["phi_global_feature_dim"], 1).to(device)
        self.phi2_local = nn.Conv1d(params["phi_local_feature_dim"] + params["phi_global_feature_dim"],
                                    params["phi_local_feature_dim"], 1).to(device)
        self.phi2_global = nn.Conv1d(params["phi_local_feature_dim"] + params["phi_global_feature_dim"],
                                     params["phi_global_feature_dim"], 1).to(device)
        self.phi3_local = nn.Conv1d(params["phi_local_feature_dim"] + params["phi_global_feature_dim"],
                                    params["phi_local_feature_dim"], 1).to(device)
        self.phi3_global = nn.Conv1d(params["phi_local_feature_dim"] + params["phi_global_feature_dim"],
                                     params["phi_global_feature_dim"], 1).to(device)

    def forward(self, x_j, x_i, e_ji, n_entries):
        def layer_processing(layer_input, local_layer, global_layer):
            local_output = F.relu(local_layer(layer_input))
            global_output = F.relu(global_layer(layer_input))
            global_output = torch.mean(global_output, dim=2, keepdim=True).repeat([1, 1, n_entries])
            layer_output = torch.cat((local_output, global_output), dim=1)
            return layer_output

        phi_input = torch.cat((x_j, x_i, e_ji), dim=1)
        r = layer_processing(phi_input, self.phi1_local, self.phi1_global)
        r = layer_processing(r, self.phi2_local, self.phi2_global)
        r = layer_processing(r, self.phi3_local, self.phi3_global)
        return r


# Information processing of one node
class Gamma1D(nn.Module):
    def __init__(self, params, input_dim, device="cpu"):
        super().__init__()
        self.gamma1_local = nn.Conv1d(input_dim, params["gamma_local_feature_dim"], 1).to(device)
        self.gamma1_global = nn.Conv1d(input_dim, params["gamma_global_feature_dim"], 1).to(device)
        self.gamma2_local = nn.Conv1d(params["gamma_local_feature_dim"] + params["gamma_global_feature_dim"],
                                      params["gamma_local_feature_dim"], 1).to(device)
        self.gamma2_global = nn.Conv1d(params["gamma_local_feature_dim"] + params["gamma_global_feature_dim"],
                                       params["gamma_global_feature_dim"], 1).to(device)
        self.gamma3_local = nn.Conv1d(params["gamma_local_feature_dim"] + params["gamma_global_feature_dim"],
                                      params["gamma_local_feature_dim"], 1).to(device)
        self.gamma3_global = nn.Conv1d(params["gamma_local_feature_dim"] + params["gamma_global_feature_dim"],
                                       params["gamma_global_feature_dim"], 1).to(device)

    def forward(self, x_i, msgs, n_entries):
        def layer_processing(layer_input, local_layer, global_layer):
            local_output = F.relu(local_layer(layer_input))
            global_output = F.relu(global_layer(layer_input))
            global_output = torch.mean(global_output, dim=2, keepdim=True).repeat([1, 1, n_entries])
            layer_output = torch.cat((local_output, global_output), dim=1)
            return layer_output

        gamma_input = torch.cat((x_i, torch.stack(msgs, dim=0).mean(dim=0)), dim=1)
        r = layer_processing(gamma_input, self.gamma1_local, self.gamma1_global)
        r = layer_processing(r, self.gamma2_local, self.gamma2_global)
        r = layer_processing(r, self.gamma3_local, self.gamma3_global)
        return r


class GNN1D(nn.Module):
    def __init__(self, params, device="cpu"):
        super().__init__()
        self.params = params
        phi_output_dim = params["phi_local_feature_dim"] + params["phi_global_feature_dim"]
        gamma_output_dim = params["gamma_local_feature_dim"] + params["gamma_global_feature_dim"]
        self.phi1 = Phi1D(params, params["num_users"] * 3, device)
        self.gamma1 = Gamma1D(params, params["num_users"] + phi_output_dim, device)
        self.phi2 = Phi1D(params, gamma_output_dim * 2 + params["num_users"], device)
        self.gamma2 = Gamma1D(params, params["num_users"] + gamma_output_dim + phi_output_dim, device)
        self.phi3 = Phi1D(params, gamma_output_dim * 2 + params["num_users"], device)
        self.gamma3 = Gamma1D(params, params["num_users"] + gamma_output_dim + phi_output_dim, device)
        self.gamma_output = nn.Conv1d(gamma_output_dim, 1, 1).to(device)

    def forward(self, gnn_input):
        def edge_processing(node_features, edge_features, phi):
            edge_output = list()
            for i, x_i in enumerate(node_features):
                output4i = list()
                for j, x_j, e_ji in zip(range(self.params["num_aps"]), node_features, edge_features):
                    if i != j:
                        output4i.append(phi(x_j, x_i, e_ji, self.params["num_users"]))
                edge_output.append(output4i)
            return edge_output

        def node_processing(node_features, edge_output, gamma):
            node_output = list()
            for x_i, msgs in zip(node_features, edge_output):
                node_output.append(gamma(x_i, msgs, self.params["num_users"]))
            return node_output

        r_edges = edge_processing(gnn_input, gnn_input, self.phi1)
        r_nodes = node_processing(gnn_input, r_edges, self.gamma1)

        r_edges = edge_processing(r_nodes, gnn_input, self.phi2)
        node_input = list()
        for r, g in zip(r_nodes, gnn_input):
            node_input.append(torch.cat((g, r), dim=1))
        r_nodes = node_processing(node_input, r_edges, self.gamma2)

        r_edges = edge_processing(r_nodes, gnn_input, self.phi3)
        for r, g in zip(r_nodes, gnn_input):
            node_input.append(torch.cat((g, r), dim=1))
        r_nodes = node_processing(node_input, r_edges, self.gamma3)
        gnn_output = list()
        for r in r_nodes:
            gnn_output.append(F.relu(self.gamma_output(r)) + 5)
        return gnn_output


# Message passing of one edge
class Phi2D(nn.Module):
    def __init__(self, params, input_dim, device="cpu"):
        super().__init__()
        self.conv1 = nn.Conv2d(input_dim, params["phi_feature_dim"] * 4, (1, 1)).to(device)
        self.conv2 = nn.Conv2d(params["phi_feature_dim"] * 4, params["phi_feature_dim"] * 4, (1, 1)).to(device)
        self.conv3 = nn.Conv2d(params["phi_feature_dim"] * 4, params["phi_feature_dim"] * 4, (1, 1)).to(device)
        self.ex = (torch.ones((params["preselection_ap"], params["preselection_ap"])) -
                   torch.eye(params["preselection_ap"])).requires_grad_().to(device)
        self.n_cat1 = params["num_users"] - 1
        self.n_cat2 = params["num_users"] - 1
        self.n_cat3 = (params["num_users"] - 1) ** 2
        self.feature_dim = params["phi_feature_dim"]
        self.num_users = params["num_users"]

    def forward(self, x_j, x_i, e_ji):
        def postprocess_layer(conv_output):
            features_c0 = conv_output[:, : self.feature_dim, :, :]
            features_c1 = self.ex @ conv_output[:, self.feature_dim: (self.feature_dim * 2), :, :] / self.n_cat1
            features_c2 = conv_output[:, (self.feature_dim * 2): (self.feature_dim * 3), :, :] @ self.ex / self.n_cat2
            features_c3 = (self.ex @ conv_output[:, (self.feature_dim * 3): (self.feature_dim * 4), :, :]
                           @ self.ex / self.n_cat3)
            return torch.cat((features_c0, features_c1, features_c2, features_c3), 1)

        phi_input = torch.cat((x_j, x_i, e_ji), dim=1)
        r = F.relu(self.conv1(phi_input))
        r = postprocess_layer(r)
        r = F.relu(self.conv2(r))
        r = postprocess_layer(r)
        r = self.conv3(r)
        r = postprocess_layer(r)
        return r


class Gamma2D(nn.Module):
    def __init__(self, params, input_dim, device="cpu"):
        super().__init__()
        self.conv1 = nn.Conv2d(input_dim, params["gamma_feature_dim"] * 4, (1, 1)).to(device)
        self.conv2 = nn.Conv2d(params["gamma_feature_dim"] * 4, params["gamma_feature_dim"] * 4, (1, 1)).to(device)
        self.conv3 = nn.Conv2d(params["gamma_feature_dim"] * 4, params["gamma_feature_dim"] * 4, (1, 1)).to(device)
        self.ex = (torch.ones((params["preselection_ap"], params["preselection_ap"])) -
                   torch.eye(params["preselection_ap"])).requires_grad_().to(device)
        self.n_cat1 = params["num_users"] - 1
        self.n_cat2 = params["num_users"] - 1
        self.n_cat3 = (params["num_users"] - 1) ** 2
        self.feature_dim = params["gamma_feature_dim"]
        self.num_users = params["num_users"]

    def forward(self, x_i, msgs):
        def postprocess_layer(conv_output):
            features_c0 = conv_output[:, : self.feature_dim, :, :]
            features_c1 = self.ex @ conv_output[:, self.feature_dim: (self.feature_dim * 2), :, :] / self.n_cat1
            features_c2 = conv_output[:, (self.feature_dim * 2): (self.feature_dim * 3), :, :] @ self.ex / self.n_cat2
            features_c3 = (self.ex @ conv_output[:, (self.feature_dim * 3): (self.feature_dim * 4), :, :]
                           @ self.ex / self.n_cat3)
            return torch.cat((features_c0, features_c1, features_c2, features_c3), 1)

        # Normalized msgs. Consider change it if nodes have different numbers of neighbors
        gamma_input = torch.cat((x_i, torch.stack(msgs, dim=0).mean(dim=0)), dim=1)
        r = F.relu(self.conv1(gamma_input))
        r = postprocess_layer(r)
        r = F.relu(self.conv2(r))
        r = postprocess_layer(r)
        r = self.conv3(r)
        r = postprocess_layer(r)
        return r


class GNN2D(nn.Module):
    def __init__(self, params, device="cpu"):
        super().__init__()
        self.params = params
        self.device = device
        phi_output_dim = params["phi_feature_dim"] * 4
        gamma_output_dim = params["gamma_feature_dim"] * 4
        if params["reduced_msg_input"]:
            self.phi1 = Phi2D(params, params["input_feature_dim"] * 2, device)
        else:
            self.phi1 = Phi2D(params, params["input_feature_dim"] * 3, device)
        self.gamma1 = Gamma2D(params, params["input_feature_dim"] + phi_output_dim, device)
        if params["reduced_msg_input"]:
            self.phi2 = Phi2D(params, gamma_output_dim * 1 + params["input_feature_dim"], device)
        else:
            self.phi2 = Phi2D(params, gamma_output_dim * 2 + params["input_feature_dim"], device)
        self.gamma2 = Gamma2D(params, params["input_feature_dim"] + gamma_output_dim + phi_output_dim, device)
        if params["reduced_msg_input"]:
            self.phi3 = Phi2D(params, gamma_output_dim * 1 + params["input_feature_dim"], device)
        else:
            self.phi3 = Phi2D(params, gamma_output_dim * 2 + params["input_feature_dim"], device)
        self.gamma3 = Gamma2D(params, params["input_feature_dim"] + gamma_output_dim + phi_output_dim, device)
        self.gamma_power_portion1 = nn.Conv2d(gamma_output_dim, gamma_output_dim, (1, 1)).to(device)
        self.gamma_power_portion2 = nn.Conv2d(gamma_output_dim, 1, (1, 1)).to(device)
        self.sigmoid = nn.Sigmoid()
        self.gamma_total_power1 = nn.Conv2d(gamma_output_dim, gamma_output_dim, (1, 1)).to(device)
        self.gamma_total_power2 = nn.Conv2d(gamma_output_dim, 1, (1, 1)).to(device)

    def forward(self, gnn_input, selection):
        def edge_processing(node_features, edge_features, phi):
            place_holder = torch.zeros((self.params["batch_size"], 0,
                                        self.params["preselection_ap"], self.params["preselection_ap"])).to(self.device)
            if self.params["reduced_msg_input"]:
                raw_edge_output = list()
                for x_j, e_ji, s in zip(node_features, edge_features, selection):
                    raw_edge_output.append(s @ phi(x_j, place_holder, e_ji) @ s.transpose(2, 3))
                edge_output = list()
                for i in range(self.params["num_aps"]):
                    edge_output.append([selection[i].transpose(2, 3) @ o @ selection[i]
                                        for j, o in enumerate(raw_edge_output) if j != i])
            else:
                edge_output = list()
                for i, x_i in enumerate(node_features):
                    output4i = list()
                    for j, x_j, e_ji in zip(range(self.params["num_aps"]), node_features, edge_features):
                        if i != j:
                            output4i.append(phi(x_j, x_i, e_ji))
                    edge_output.append(output4i)

            if len(edge_output[0]) == 0:
                edge_output.pop()
                edge_output.append([torch.zeros_like(raw_edge_output[0])])
            return edge_output

        def node_processing(node_features, edge_output, gamma):
            node_output = list()
            for x_i, msgs in zip(node_features, edge_output):
                node_output.append(gamma(x_i, msgs))
            return node_output

        r_edges = edge_processing(gnn_input, gnn_input, self.phi1)
        r_nodes = node_processing(gnn_input, r_edges, self.gamma1)

        r_edges = edge_processing(r_nodes, gnn_input, self.phi2)
        node_input = list()
        for r, g in zip(r_nodes, gnn_input):
            node_input.append(torch.cat((g, r), dim=1))
        r_nodes = node_processing(node_input, r_edges, self.gamma2)

        r_edges = edge_processing(r_nodes, gnn_input, self.phi3)
        for r, g in zip(r_nodes, gnn_input):
            node_input.append(torch.cat((g, r), dim=1))
        r_nodes = node_processing(node_input, r_edges, self.gamma3)
        gnn_output = list()
        for r in r_nodes:
            # gnn_output.append(F.relu(torch.diagonal(self.gamma_output(r), dim1=2, dim2=3) + 2) / 50)

            # transmit_power = F.relu(self.gamma_power_portion1(r))
            # transmit_power = torch.diagonal(self.gamma_power_portion2(transmit_power), dim1=2, dim2=3)

            transmit_power = torch.diagonal(self.gamma_power_portion2(r), dim1=2, dim2=3)

            if self.params["objective"] == "sumrate":
                # norm_transmit_power = transmit_power / (1 + transmit_power.sum(dim=[1, 2], keepdim=True))
                # gnn_output.append(norm_transmit_power)
                total_power = F.relu(self.gamma_total_power1(r))
                total_power = torch.sigmoid(self.gamma_total_power2(total_power).mean(dim=[1, 2, 3],
                                                                                      keepdim=True))[:, :, :, 0]
                transmit_power_normed = F.softmax(transmit_power, dim=2) * total_power * self.params["pmax"]
                # transmit_power_normed = transmit_power * total_power * self.params["pmax"]
                # transmit_power_normed = torch.sigmoid(transmit_power) * total_power * self.params["pmax"]
                gnn_output.append(transmit_power_normed)
                # gnn_output.append(torch.sigmoid(transmit_power) * self.params["pmax"])
                # gnn_output.append(transmit_power)
            elif self.params["objective"] == "power":
                gnn_output.append(torch.sigmoid(transmit_power) * self.params["pmax"])
            elif self.params["objective"] == "ee":
                # gnn_output.append(torch.nn.Softplus()(transmit_power))
                gnn_output.append(transmit_power)
        return gnn_output


class GNNGlobal(nn.Module):
    def __init__(self, params, device = 'cpu') -> None:
        super().__init__()
        self.gnn_a = GNN2D(params, device).to(device)
        self.gnn_b = GNN2D(params, device).to(device)
        self.params = params

    def forward(self, gnn_input, selection):
        a = torch.stack(self.gnn_a(gnn_input, selection))
        if self.params["objective"] == "sumrate":
            a = torch.nn.Softplus()(a - 2)*self.params["pmax"]
        elif self.params["objective"] == "ee":
            a = torch.nn.Softplus()(a-2)

        bma = torch.stack(self.gnn_b(gnn_input, selection))
        if self.params["objective"] == "sumrate":
            bma = torch.nn.Softplus()(bma + 4)*self.params["pmax"]
        elif self.params["objective"] == "ee":
            bma = torch.nn.Softplus()(bma + 4)
        bma = torch.maximum(bma, torch.tensor(0.00000001))
        b = bma + a
        pi_distribution = torch.distributions.uniform.Uniform(a, b)
        entropy = pi_distribution.entropy().sum(dim=(0,-1,-2))
        action = pi_distribution.rsample()
        sum_power = torch.maximum(action.sum(dim=-1, keepdims=True), torch.ones(1).to(self.gnn_a.device))
        power = action/sum_power

        return power, entropy, a, b  


class CFData(Dataset):
    def __init__(self, params, path="", device="cpu"):
        super().__init__()
        self.params = params
        self.path = path
        self.device = device
        self.samples_per_chunk = self.params["num_samples_chunks"]
        self.total_chunks = int(np.ceil(self.params["num_data_samples"]/self.samples_per_chunk))
        self.chunk_num = 0
        self.channels, self.rate_requirements, self.selection_matrices =  self.load_chunk(self.chunk_num, self.device, self.path)

        
    def __getitem__(self, item):
        item = int(item % self.samples_per_chunk)
        self.counter += 1
        if self.counter > self.samples_per_chunk:
            self.chunk_num +=1      # iterate over the number of chunks
            if self.chunk_num < self.total_chunks:
                self.channels, self.rate_requirements, self.selection_matrices = self.load_chunk(self.chunk_num, self.device, path=self.path)
        
        return (item, [d[item, :, :, :] for d in self.channels],
                self.rate_requirements[item, :],
                [d[item, :, :] for d in self.selection_matrices])

    def __len__(self):
        return self.params["num_data_samples"]

    def preselect(self, channel):
        signal_channel = torch.diag(channel)
        _, indices = torch.topk(signal_channel, self.params["preselection_ap"])
        selection_mat = np.zeros((self.params["num_users"], self.params["preselection_ap"]), dtype=np.float32)
        for i, index in enumerate(indices):
            selection_mat[index, i] = 1
        selection_mat = torch.tensor(selection_mat)

        return selection_mat

    def load_chunk(self, chunk_num, device, path =""):
        """ Loads a previously saved chunk to memory.

        Args:
            chunk_num (int): The chunk id (integer) to be restored.
            path (str, optional): Optional. The path, where the chunks are stored.

        Returns:
            List, Tensor, List: Channels as a list with L items and each item of the shape n x VirtualUsers x VirtualUsers, rate requirement per user, selection matrix per AP with same configuration as channels.
        """        
        
        channels, rate_req, selection = CFData.load_chunk_num(chunk_num, dir_path=path)
        channels, rate_req, selection = torch.from_numpy(channels).to(device).unbind(), torch.from_numpy(rate_req).to(device), torch.from_numpy(selection).to(device).unbind()

        channels = [c[:, None, :, :] for c in channels]
        selection = [m[:, None, :, :] for m in selection]  # To enable broadcast
        self.counter = 0        # reset the counter for number of items sampled

        return channels, rate_req, selection


def update_kappa(entropies, previous_kappa):
    lr_kappa = 1e-6
    if len(entropies) < 10:
        return previous_kappa
    elif entropies[-1] >= np.mean(entropies[:-1]):
        return previous_kappa + lr_kappa
    else:
        return F.relu(previous_kappa - lr_kappa / 2)
    
def global_objective(sumrate, deficiency, kappa, entropy, a, b, params, device):
    # penalty = deficiency.clone()
    penalty = torch.empty_like(deficiency).copy_(deficiency)

    #test with pmax per user
    penalty += torch.mean(torch.maximum(torch.zeros(1).to(device), b.sum(axis=-1) - params["pmax"]),dim=(0,-2,-1))
    penalty += torch.mean(torch.maximum(torch.zeros(1).to(device), a.sum(axis=-1) - params["pmax"]),dim=(0,-2,-1))
    # penalty += torch.sum(torch.maximum(torch.zeros(1).to(device), b - params["pmax"]), dim=(0,-2,-1))
    # penalty += torch.mean(torch.maximum(torch.zeros(1).to(device), b - params["pmax"]), dim=(0,-2,-1))
    # penalty += torch.sum(torch.maximum(torch.zeros(1).to(device), -a), dim=(0,-2,-1))
    # penalty += torch.mean(torch.maximum(torch.zeros(1).to(device), -a), dim=(0,-2,-1))
    objective = sumrate*(penalty==0) - kappa*entropy - 100*penalty
    return objective, penalty

def sumrate(channel, power, selection, params):
    # power = torch.clip(power,0)
    rss = [c[:, 0, :, :] * (p @ s[:, 0, :, :].transpose(1, 2)) for c, p, s in zip(channel, power, selection)]
    rss = torch.sum(torch.stack(rss, dim=0), dim=0)
    sum_rate = 0
    for user_idx in range(params["num_users"]):
        sum_rate = sum_rate + torch.log2(1 + rss[:, user_idx, user_idx] /
                                         (torch.sum(rss[:, user_idx, :], dim=1) - rss[:, user_idx, user_idx] +
                                          params["noise_power"]))
    return sum_rate

def sum_ee(channel, power, selection, params):
    pc = 4
    sr = sumrate(channel,power,selection, params)
    if isinstance(power,list):
        power_consumtion = torch.stack(power,dim=3).sum(dim=[1,2,3])
    else:
        power_consumtion = power.sum(dim=[0,2,3])
    ee = sr/(pc+power_consumtion)
    return ee

def global_ee(a, b, kappa, entropy, channel, power, selection, params, device):
    ee = sum_ee(channel, power, selection, params)
    penalty = 0
    penalty += torch.mean(torch.maximum(torch.zeros(1).to(device), b.sum(axis=-1)),dim=(0,-2,-1))
    penalty += torch.mean(torch.maximum(torch.zeros(1).to(device), a.sum(axis=-1)),dim=(0,-2,-1))
    objective = ee - 100*penalty - kappa*entropy
    return objective, penalty

def power_st_rates(channels, power, selection, required_rates, params, device="cpu"):
    deficiency = compute_rate_deficiency(channels, power, selection, required_rates, params, device)
    power_consumption = torch.stack(power, dim=3).sum(dim=[1, 2, 3])
    return deficiency * 1000 + power_consumption * (deficiency == 0)


def compute_rate_deficiency(channels, power, selection, required_rates, params, device="cpu"):
    # power = torch.clip(power,0)
    rss = [c[:, 0, :, :] * (p @ s[:, 0, :, :].transpose(1, 2)) for c, p, s in zip(channels, power, selection)]
    rss = torch.sum(torch.stack(rss, dim=0), dim=0)
    deficiency = 0
    for user_idx in range(params["num_users"]):
        rate = torch.log2(1 + rss[:, user_idx, user_idx] /
                          (torch.sum(rss[:, user_idx, :], dim=1) - rss[:, user_idx, user_idx] + params["noise_power"]))
        deficiency += torch.maximum(torch.zeros(1).to(device), required_rates[:, user_idx] - rate)
    return deficiency


def power_constraint(power, params):
    penalty = 0
    for p in power:
        penalty += torch.maximum(torch.zeros(1), p.sum(dim=[1, 2]) - params["pmax"])
    return penalty


def aggregate_input(channels, required_rates, selection, params):
    channels = [(c - params["mean_channel"]) / params["std_channel"] for c in channels]
    # if params["objective"] == "sumrate":
    if params["input_feature_dim"] == 1:
        channels = [s.transpose(dim0=-1, dim1=-2) @ c @ s for s, c in zip(selection, channels)]
        return channels
        
    # elif params["objective"] == "power":
    elif params["input_feature_dim"] == 2:
        required_rates = torch.nan_to_num((required_rates[:, None, :, None] - params["mean_rate_requirements"])
                          / params["std_rate_requirements"]).repeat((1, 1, 1, params["num_users"]))
        gnn_input = [torch.cat((c, required_rates), dim=1) for c in channels]
        gnn_input = [s.transpose(2, 3) @ g @ s for s, g in zip(selection, gnn_input)]
        return gnn_input
    
    else:
        print('input feature dim not recognised')
        return None


def schatten_norm(power, p):
    return (power ** p).sum(dim=[1, 2]) ** (1 / p)


def calc_loss(channels, powers, selection, params):
    sr = sumrate(channels, powers, params)
    powers_stack = torch.stack(powers, dim=1)

def gen_pure_channels(L, N, K, num_realizations):
    quota_ue = [params["num_min_aps"]] * K  # max amount of APs per UE
    quota_ap = [params["preselection_ap"]] * L  # max amount of UEs per AP
    R = torch.eye(N)
    AP_pos = gen_grid_locations(L)
    UE_pos = gen_locations(K)
    
    ap_ue_dist = compute_dist(AP_pos, UE_pos)
    channel_gains = gains(ap_ue_dist)

    channel = []
    # R_sqrt = torch.zeros(L,K,N,N, dtype=torch.complex64)
    # spatial_R = list()
    for l in range(L):
        H = torch.randn(N, num_realizations, K) + 1j * torch.randn(N, num_realizations, K)
        channel_k = []
        for k in range(K):
            spatial_R = db2pow(channel_gains[l, k]) * R
            # print(spatial_R.shape)
            R_sqrt = torch.from_numpy(sp.sqrtm(spatial_R).astype('complex64'))
            # print(R_sqrt.unsqueeze(0).shape)
            # R_sqrt = R_sqrt.unsqueeze(0).repeat(num_realizatons,1,1)
            # print(H[:,l,:,k].shape,R_sqrt.shape)
            channel_k.append(torch.sqrt(torch.tensor(0.5)) * R_sqrt @ H[:, :, k])
        # channel.append(torch.stack(channel_k,dim=2))
        channel_l = torch.stack(channel_k, dim=2)
        channel.append(channel_l)
        # print(channel_l.shape)

    if params["preselection"].lower() == 'dom':
        problem = AssignmentProblem(channel_gains.T, quota_ue, quota_ap)
        dom = torch.tensor(problem.solve('Dom'), dtype=torch.float32).T
        ap_to_ue = torch.nonzero(dom)

        return torch.stack(channel), dom
        # for l in range(L):
        #     v_user_to_user = torch.zeros((params["preselection_ap"], params["num_users"]))
        #     users_per_ap = ap_to_ue[ap_to_ue[:, 0] == l][:, 1]
        #     for i, j in enumerate(users_per_ap):
        #         v_user_to_user[i, j] = 1
        #     selection_matrices.append(v_user_to_user)
    else: 
        return torch.stack(channel), None


class CFData_v0(Dataset):
    def __init__(self, params, device="cpu"):
        super().__init__()
        self.params = params

        data_available = False
        if data_available:
            self.channels = list()
            for i in range(params["num_aps"]):
                c = torch.load(params["channels_path"] + "_{i}.pt".format(i=i))[:params["num_data_samples"], :, :].to(device)
                self.channels.append(c)
            self.rate_requirements = torch.load(params["required_rates_path"]).to(device)
        else:
            self.channels = self.gen_channels_v2(params["num_aps"],
                                                 params["num_antennas"],
                                                 params["num_users"],
                                                 params["num_data_samples"], device)

            self.rate_requirements = torch.rand(params["num_data_samples"], params["num_users"]) * 0.8

            # for i, c in enumerate(self.channels):
            #     torch.save(c, params["channels_path"] + "_{i}.pt".format(i=i))

            # torch.save(self.rate_requirements, params["required_rates_path"])

        self.selected_channels = list()
        self.selection_matrices = list()
        for ap_idx in range(params["num_aps"]):
            selections = list()
            for sample_idx in range(params["num_data_samples"]):
                selection_mat = self.preselect(self.channels[ap_idx][sample_idx, 0, :, :])
                selections.append(selection_mat)

            self.selection_matrices.append(torch.stack(selections, dim=0).to(device))
        self.selection_matrices = [m[:, None, :, :] for m in self.selection_matrices]  # To enable broadcast

    def __getitem__(self, item):
        return ([d[item, :, :, :] for d in self.channels],
                self.rate_requirements[item, :],
                [d[item, :, :] for d in self.selection_matrices])

    def __len__(self):
        return self.channels[0].shape[0]

    def preselect(self, channel):
        signal_channel = torch.diag(channel)
        _, indices = torch.topk(signal_channel, self.params["preselection_ap"])
        selection_mat = np.zeros((self.params["num_users"], self.params["preselection_ap"]), dtype=np.float32)
        for i, index in enumerate(indices):
            selection_mat[index, i] = 1
        selection_mat = torch.tensor(selection_mat)

        return selection_mat

    def gen_channel(self, L, N, K, num_realizations, device="cpu"):
            # K: user
            # L: AP
            R = torch.eye(N)
            AP_pos = gen_grid_locations(L)
            UE_pos = gen_locations(K)
            ap_ue_dist = compute_dist(AP_pos,UE_pos)
            channel_gains = gains(ap_ue_dist)
            channel = []
            # R_sqrt = torch.zeros(L,K,N,N, dtype=torch.complex64)
            # spatial_R = list()
            for l in range(L):
                H = torch.randn(N,num_realizations,K) + 1j * torch.randn(N,num_realizations,K)
                channel_k = []
                for k in range(K):
                    spatial_R = db2pow(channel_gains[l,k])*R
                    # print(spatial_R.shape)
                    R_sqrt = torch.from_numpy(sp.sqrtm(spatial_R).astype('complex64'))
                    # print(R_sqrt.unsqueeze(0).shape)
                    # R_sqrt = R_sqrt.unsqueeze(0).repeat(num_realizatons,1,1)
                    # print(H[:,l,:,k].shape,R_sqrt.shape)
                    channel_k.append(torch.sqrt(torch.tensor(0.5))*R_sqrt@H[:,:,k])
                # channel.append(torch.stack(channel_k,dim=2))
                channel_l = torch.stack(channel_k,dim=2)
                # print(channel_l.shape)
                V = torch.transpose(torch.conj(channel_l),dim0=0,dim1=2)
                # print(V.shape)
                e_channel = []
                for n in range(num_realizations):
                    v = V[:, n, :]
                    e_channel.append((v / torch.sqrt((v.abs() ** 2).sum(dim=1, keepdim=True)) @
                                      channel_l[:,n,:]).transpose(0, 1))
                e_channel = torch.stack(e_channel,dim=0)
                e_channel = torch.unsqueeze(e_channel,1)
                # print(e_channel[0])
                channel.append(torch.square(torch.abs(e_channel)).to(device))
            return channel

    def gen_channels_v2(self, L, N, K, num_realizations, device="cpu"):
        channels = list()
        for _ in range(L):
            channels.append(list())
        R = torch.eye(N)
        AP_pos = gen_grid_locations(L)
        for n in range(num_realizations):
            UE_pos = gen_locations(K)
            ap_ue_dist = compute_dist(AP_pos,UE_pos)
            channel_gains = gains(ap_ue_dist)
            # R_sqrt = torch.zeros(L,K,N,N, dtype=torch.complex64)
            # spatial_R = list()
            for l in range(L):
                H = torch.randn(N, 1, K) + 1j * torch.randn(N, 1, K)
                channel_k = []
                for k in range(K):
                    spatial_R = db2pow(channel_gains[l,k])*R
                    # print(spatial_R.shape)
                    R_sqrt = torch.from_numpy(sp.sqrtm(spatial_R).astype('complex64'))
                    # print(R_sqrt.unsqueeze(0).shape)
                    # R_sqrt = R_sqrt.unsqueeze(0).repeat(num_realizatons,1,1)
                    # print(H[:,l,:,k].shape,R_sqrt.shape)
                    channel_k.append(torch.sqrt(torch.tensor(0.5))*R_sqrt@H[:,:,k])
                # channel.append(torch.stack(channel_k,dim=2))
                channel_l = torch.stack(channel_k,dim=2)
                # print(channel_l.shape)
                V = torch.transpose(torch.conj(channel_l),dim0=0,dim1=2)
                # print(V.shape)
                e_channel = []
                v = V[:, 0, :]
                e_channel.append((v / torch.sqrt((v.abs() ** 2).sum(dim=1, keepdim=True)) @
                                  channel_l[:,0,:]).transpose(0, 1))
                e_channel = torch.stack(e_channel,dim=0)
                e_channel = torch.unsqueeze(e_channel,1)
                # print(e_channel[0])
                channels[l].append(torch.square(torch.abs(e_channel)))

        for l in range(L):
            channels[l] = torch.cat(channels[l], dim=0).to(device)

        return channels
