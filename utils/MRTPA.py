import numpy as np
from utils.CFDataGenerator import CFDataGenerator

class MRTPA():
    """Assign all power to the user with best channel.
    """    
    def __init__(self,pmax) -> None:
        """

        Parameters
        ----------
        pmax : float or list
            A single value or list of maximum available power at each AP.
        """        
        self.pmax = pmax if not np.isscalar(pmax) else [pmax]

    def __call__(self, Datagenerator:CFDataGenerator):
        """ This class can be called on the data generator object of the cell free massive MIMO network.

        Parameters
        ----------
        Datagenerator : CFDataGenerator
            Data generator class for the cell free massive MIMO network.

        Returns
        -------
        list
            A list of power allocation arrays for every pmax provided.
        """        
        self.channels = Datagenerator.effective_channels
        self.bu = self.best_user(self.channels)
        self.pa = [self.allocate_power(self.bu,pmax) for pmax in self.pmax]
        return self.pa

    def best_user(self, channels):
        """Find the best user based on the channel gains

        Parameters
        ----------
        channels : np.ndarray
            Channel information of each user per AP. Dimension: L x n x K x K

        Returns
        -------
        np.ndarray
            Masked array of best user per AP per realization.
        """        
        gains = np.diagonal(channels,axis1=-2,axis2=-1)
        max_gains = np.amax(gains,axis=-1,keepdims=True)
        bu_mask = np.ma.masked_where(gains==max_gains, gains).mask
        return bu_mask
    
    def allocate_power(self, bu_mask, power):
        """allocate max power to the best user provided and 0 to remaining users

        Parameters
        ----------
        bu_mask : np.ndarray
            masked array with the best user per AP l and realization n is 1 and the other users are 0
        power : float
            maximum power available at each AP (usually a scalar value).

        Returns
        -------
        np.ndarray
            power allocation of K users per n realizations and L APs.
        """        
        return bu_mask*power
